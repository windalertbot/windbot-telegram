from interfaces import IWindProvider, IWindbot

from telegram.wind_bot import WindBot
from database.database import Database
from weather import Windprovider


class Factory():
    # since lambda is single call - cahching won't be problem
    __WINDBOT = None
    __DATABASE = None
    __WINDPROVIDER = None

    @staticmethod
    def Init() -> None:
        """
            In some cases looks like AWS re-uses state, so caching MAY be a problem
        """
        Factory.__WINDBOT = None
        Factory.__DATABASE = None
        Factory.__WINDPROVIDER = None

    @staticmethod
    def AccessWindbot(token: str) -> IWindbot:
        if not Factory.__WINDBOT:
            Factory.__WINDBOT = WindBot(token)
        return Factory.__WINDBOT

    @staticmethod
    def AccessDatabase(endpoint_url=None) -> Database:
        if not Factory.__DATABASE:
            Factory.__DATABASE = Database(endpoint_url)
        return Factory.__DATABASE

    @staticmethod
    def AccessWindprovider() -> IWindProvider:
        if not Factory.__WINDPROVIDER:
            Factory.__WINDPROVIDER = Windprovider()
        return Factory.__WINDPROVIDER
