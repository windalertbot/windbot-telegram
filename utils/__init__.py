import json
import time

from typing import Tuple
from errors import ParseError

from datetime import datetime
from dateutil import tz


class WindDirectionsEmoji:
    N = '⬇️'
    E = '⬅️'
    S = '⬆️'
    W = '➡️'

    SW = '↗️'
    NW = '↘️'
    NE = '↙️'
    SE = '↖️'

    DIR_CENTER = '⏺'
    DIR_NONE = '◼️'


def parseWindDir(wind_dir_str: str) -> Tuple[int, int]:
    wind_dir = wind_dir_str.split('-')
    if len(wind_dir) < 2:
        raise ParseError("Invalid format for wind direction 😕.\nFormats example: 180-270, 340-15, NE-SE")

    start = from_dir_to_deg(wind_dir[0])
    end = from_dir_to_deg(wind_dir[1])

    return [start, end]


def from_dir_to_deg(inp: str):
    try:
        val = int(inp)
        if val < 0 or val > 359:
            raise ParseError(f"'{val}' is not valid wind direction in degrees. Should be in [0..359] range")
        return val
    except ValueError:
        return from_dir_str_to_deg(inp)


def from_dir_str_to_deg(inp: str):
    wdir = inp.upper()
    Notrh, East, South, West = 0, 90, 180, 270
    eighth = 23
    quater = 45

    if wdir == "N":
        return Notrh
    elif wdir == "NNE":
        return Notrh + eighth
    elif wdir == "NE":
        return Notrh + quater
    elif wdir == "ENE":
        return Notrh + quater + eighth

    elif wdir == "E":
        return East
    elif wdir == "ESE":
        return East + eighth
    elif wdir == "SE":
        return East + quater
    elif wdir == "SSE":
        return East + quater + eighth

    elif wdir == "S":
        return South
    elif wdir == "SSW":
        return South + eighth
    elif wdir == "SW":
        return South + quater
    elif wdir == "WSW":
        return South + quater + eighth

    elif wdir == "W":
        return West
    elif wdir == "WNW":
        return West + eighth
    elif wdir == "NW":
        return West + quater
    elif wdir == "NNW":
        return West + quater + eighth
    else:
        raise ParseError("'{0}' is not recognizable wind direction".format(inp))


def getWindDirEmoji(input_dir_str: str) -> str:
    single_dir = None
    try:
        input_dir = parseWindDir(input_dir_str)
    except BaseException:
        if input_dir_str:
            try:
                single_dir = from_dir_to_deg(input_dir_str)
            except BaseException:
                pass
        input_dir = None

    dir_to_emoji = [
        (315, WindDirectionsEmoji.NW),
        (0, WindDirectionsEmoji.N),
        (45, WindDirectionsEmoji.NE),

        (270, WindDirectionsEmoji.W),
        (-1, WindDirectionsEmoji.DIR_CENTER),
        (90, WindDirectionsEmoji.E),

        (225, WindDirectionsEmoji.SW),
        (180, WindDirectionsEmoji.S),
        (135, WindDirectionsEmoji.SE),
    ]

    def isInside(wdir: int, input_dir: Tuple[int, int]):
        if not input_dir:
            return None

        if input_dir[0] < input_dir[1]:
            return wdir >= input_dir[0] and wdir <= input_dir[1]
        else:
            return wdir >= input_dir[0] or wdir <= input_dir[1]

    string = ''
    new_line_counter = 0
    for wdir, emoji in dir_to_emoji:
        new_line_counter += 1

        char = WindDirectionsEmoji.DIR_NONE
        if wdir < 0:
            string += WindDirectionsEmoji.DIR_CENTER
            continue

        if input_dir is not None:
            char = emoji if isInside(wdir, input_dir) else char
        elif single_dir is not None:
            char = emoji if 0 <= (wdir - single_dir) < 45 else char

        string += char
        if new_line_counter % 3 == 0:
            string += '\n'

    return string


def parseTimezone(timezone: str) -> str:
    if tz.gettz(timezone) is not None:
        return timezone

    gmt_now = datetime.now(tz.gettz('GMT')).hour
    usr_now = int(timezone)
    if usr_now < 0 or usr_now > 23:
        raise ParseError("Hour value: '{0}' is invalid. Must be in [0..23] range".format(usr_now))

    # normalize GMT into 12
    nml_offset = 12 - gmt_now
    # get user's time when GMT is 12
    usr_gmt_12 = (usr_now + nml_offset) % 24

    offset = usr_gmt_12 - 12
    if offset > 0:
        return "GMT+{0}".format(abs(offset))
    else:
        return "GMT-{0}".format(abs(offset))


def parseNotificationTime(awakeTime: str) -> Tuple[int, int]:
    valid = json.loads(awakeTime)

    morning = int(valid[0])
    if morning < 0 or morning > 23:
        raise ParseError(f"Invalid morning value: '{morning}'. Awake time should be defined in 24h format.")

    evening = int(valid[1])
    if evening < 0 or evening > 23:
        raise ParseError(f"Invalid evening value: '{evening}'. Sleep time should be defined in 24h format.")

    return [min(evening, morning), max(evening, morning)]


class TimeMeasure:
    def __init__(self):
        super().__init__()
        self.__begin = None
        self.__end = None

    def __enter__(self):
        self.__begin = time.time()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__end = time.time()

    @property
    def elapsed_ms(self) -> int:
        if not self.__begin:
            return 0

        end = self.__end if self.__end else time.time()
        return int((end - self.__begin) * 1000)
