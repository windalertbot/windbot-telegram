from tests.telegram.conftest import assert_sent_message

from telegram.callback_actions.remove_alert import Strings


def test_windbot_remove_command_info(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/remove"))
    assert_sent_message(Strings.ActionRemoveAlert.NO_SPOTS_TO_REMOVE)


def test_windbot_remove_command_invalid(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/remove asdasdasdasd"))
    assert_sent_message("Alert with id 'asdasdasdasd' wasn't even found")


def test_windbot_remove_command_valid(wind_bot, telegram_env, create_webhook_message):
    # add first
    wind_bot.OnWebHook(create_webhook_message("/alert a 1 2-3"))
    assert_sent_message("Alert was set")

    # remove then
    wind_bot.OnWebHook(create_webhook_message("/remove a"))
    assert_sent_message("Alert was removed")


def test_windbot_command_remove_user_not_found(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/remove xxx", 7787))
    assert_sent_message("You wasn't found in database")
