from telegram.callback_actions import Strings
from errors import CommonErrors


def test_callback_action_wearther(wind_bot, telegram_env, create_callback_action_message,
                                  assert_message, assert_markup_button, assert_last_message_deleted):
    action = create_callback_action_message(Strings.ActionWeather.NAME, {})
    wind_bot.OnWebHook(action)
    assert_message(CommonErrors.NO_WEATHER_DATA)
    assert_markup_button(Strings.Keyboard.BUTTON_CLOSE)

    action = create_callback_action_message(Strings.ActionWeather.NAME, {
        Strings.ActionClose.DATA_ACTION_CLOSE: True,
        Strings.ActionBase.DATA_ORIGIN_NAME: 123123
    })
    wind_bot.OnWebHook(action)
    assert_last_message_deleted()
