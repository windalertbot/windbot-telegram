from telegram.callback_actions import Strings
from tests.telegram.conftest import assert_sent_message


def test_windbot_find_command(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/find"))
    assert_sent_message('Usage: `/find')

    wind_bot.OnWebHook(create_webhook_message("/find 1"))
    assert_sent_message('You need to enter at least 4')

    wind_bot.OnWebHook(create_webhook_message("/find 1 2"))
    assert_sent_message('You need to enter at least 4')


def test_windbot_find_command_not_found(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/find none"))
    assert_sent_message('Nothing was found')


def test_windbot_find_command_too_big(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/find This is string more that 16 symbols"))
    assert_sent_message('too big')


def test_windbot_find_command_found(wind_bot, telegram_env, create_webhook_message, assert_markup_callback_data):
    wind_bot.OnWebHook(create_webhook_message("/find hello"))
    assert_sent_message('Id_hello')

    wind_bot.OnWebHook(create_webhook_message('/find "hello world"'))
    assert_sent_message('Id_hello world')

    wind_bot.OnWebHook(create_webhook_message("/find hello world"))
    assert_sent_message('Id_hello world')


def test_windbot_find_command_found_single_spot(wind_bot, telegram_env, create_webhook_message,
                                                windprovider_fixture, assert_markup_callback_data):
    wind_bot.OnWebHook(create_webhook_message("/find single"))
    assert_markup_callback_data(Strings.ActionAddAlert.NAME)
    assert_markup_callback_data('Country_single', False)
    assert_markup_callback_data('Id_single')


def test_windbot_find_command_found_single_country(wind_bot, telegram_env, create_webhook_message,
                                                   windprovider_fixture, assert_markup_callback_data):
    wind_bot.OnWebHook(create_webhook_message("/find same country"))
    assert_markup_callback_data(Strings.ActionFindAlert.NAME)
    assert_markup_callback_data('Country_same')
    assert_sent_message('Id_same country')
    assert_sent_message('Id_same country1')
