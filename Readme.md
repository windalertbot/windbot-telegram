#### Disclaimer
> This is non-commercial project. I did it mostly for myself, but everything is public available and anybody free to use this bot.
>
> Python is not my _main_ programming language (C#, C++ are), code quality here is not the best for sure. I really hate python, and this project helped me to understand why 🙂.

# What is it?

This is a [Telegram](https://telegram.org/) bot which can send you a message if wind requirements are met in specific spot. If you are doing surfing (wind, kite) or yahting or whatever that invlolves wind, and the place where you doing it (or nearby) has meteostation - you can set an alert via this bot.

Try it: [Wind Alert Bot](https://t.me/windalertbot)

# How it works?

 * Code deployed to AWS Labmda via gitlab CI/CD.
 * Bot receive updates from Telegram via webhook.
 * AWS APIGateway used to set up the webhook endpoint and it proxies request to lambda.
 * AWS DynamoDb used for persistent storage.
 * AWS EventBridge used for triggering update action.

### Unit tests
 - Need to have windows machine.
 - Need to download [DynamoDBLocal](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html)

### Windprovider
 Well, since I didn't find any public API for retrieving weather info, and web-site parsing it's "not da way". I had to improvise... This kind of improvisation cannot be public available, thats why `weather` folder is private submolue. However, you can do your own `windprovider`, just look at the [interface](interfaces/__init__.py) it should implement.

### TODO
 Lots of stuff actually. Better UI/UX, more complex rules, snooze buttons on alerts... Most likely it won't be done.
