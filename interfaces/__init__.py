from data.spot import SpotInfo, SpotSearchResult, SpotWeather
from typing import Dict, List, Any
from telebot.types import Message


class IWindbot(object):  # pragma: no cover
    """
        An interface which can be used by different components in project
        to send/edit/delete messages
    """
    def SendMessage(self, chat_id: int, text: str, can_reply: bool = True, **kwargs):
        raise NotImplementedError("Interface method should be implemented")

    def UpdateMessage(self, message: Message, edit_args: Dict[str, Any], can_reply: bool = True):
        raise NotImplementedError("Interface method should be implemented")

    def DeleteMessage(self, chat_id: int, message_id: int, can_reply: bool = True):
        raise NotImplementedError("Interface method should be implemented")


class IWindProvider(object):  # pragma: no cover
    """
        An interface which can be used by different components in project
        to access actual wind data
    """
    def getSpotInfo(self, spot: str) -> SpotInfo:
        raise NotImplementedError("Interface method should be implemented")

    def searchSpotByName(self, name: str) -> SpotSearchResult:
        raise NotImplementedError("Interface method should be implemented")

    def getCurrentState(self, spots_id: List[str]) -> List[SpotWeather]:
        raise NotImplementedError("Interface method should be implemented")
