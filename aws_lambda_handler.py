import os
import json
import logging
import utils

from aws_lambda_data import LambdaEvent, LambdaEventAction
from aws_lambda_factory import Factory

from actions.collect_spots import UserNotificationCollector
from actions.collect_weather import WeatherCollector

telegram_token = os.getenv('TELEGRAM_TOKEN')
cloudwatch_token = os.getenv('CLOUDWATCH_TOKEN')

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def authorize(lambda_event: LambdaEvent):
    if lambda_event.token not in [telegram_token, cloudwatch_token]:
        raise KeyError("Invalid or missing token")


def check_environment():
    if telegram_token is None:
        raise EnvironmentError("Missing TELEGRAM_TOKEN env variable!")

    if cloudwatch_token is None:
        raise EnvironmentError("Missing CLOUDWATCH_TOKEN env variable!")


def lambda_handler(_event, _context):
    lambda_event = LambdaEvent(_event)

    try:
        # pre-check
        check_environment()

        # clear cache
        Factory.Init()

        # create event and authorize
        authorize(lambda_event)

        # do action
        action_handlers = {
            LambdaEventAction.WEBHOOK: on_webhook_action,
            LambdaEventAction.UPDATE: on_update_action,
        }

        handler = action_handlers.get(lambda_event.action)
        if handler is None:
            raise KeyError("No handlers for action:{0}".format(lambda_event.action))

        return handler(lambda_event)

    except Exception as error:
        logger.error(f"""Unhadled error in lamda handler: {error.__class__.__name__}
Action:{lambda_event.action}
{error}""")
        return {
            'status': 500,
            'body': json.dumps({'Error': "{0}: {1}".format(error.__class__.__name__, error)})
        }


def on_webhook_action(lambda_event: LambdaEvent):
    bot = Factory.AccessWindbot(telegram_token)
    reply = bot.OnWebHook(lambda_event.payload)

    reply['status'] = 200
    return reply


def on_update_action(lambda_event: LambdaEvent):
    logger.info("Update action triggered")

    # prereqs
    database = Factory.AccessDatabase()
    bot = Factory.AccessWindbot(telegram_token)
    windprovider = Factory.AccessWindprovider()

    # get user from database
    with utils.TimeMeasure() as dbExecutionTime:
        users = database.getUsersToNotify()

    # collected weather from all spots
    with utils.TimeMeasure() as wpExecutionTime:
        weather = WeatherCollector.collect(windprovider, users)

    # notifications for users
    with utils.TimeMeasure() as tgExecutionTime:
        notifications = UserNotificationCollector.collect(users, weather)
        bot.SendNotifications(notifications)

    logger.info(f"""
Update action triggered for {len(users)} users.
Stats:
    Database access time: {dbExecutionTime.elapsed_ms}ms
    Windprovider api time: {wpExecutionTime.elapsed_ms}ms
    Telegram sending notifications time: {tgExecutionTime.elapsed_ms}ms
""")

    return {
        'status': 200,
        'body': 'ok'
    }
