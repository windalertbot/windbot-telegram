import pytest

from datetime import datetime, timedelta

from telegram.callback_actions import Strings

from tests.telegram.conftest import assert_sent_message


@pytest.fixture
def create_set_timezone_action(create_callback_action_message):
    def _create_set_timezone_action(mode, current, origin=None, confirm=False):
        return create_callback_action_message(Strings.ActionTimezone.NAME, {
            Strings.ActionTimezone.DATA_MODE_NAME: mode,
            Strings.ActionTimezone.DATA_CURRENT_VALUE_NAME: current,
            Strings.ActionTimezone.DATA_CONFIRMATION_NAME: confirm,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })
    return _create_set_timezone_action


def test_callback_action_settings_set_timezone_init(wind_bot, telegram_env, create_set_timezone_action, caplog):
    action_init = create_set_timezone_action(None, None)
    wind_bot.OnWebHook(action_init)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_TZ_INIT)


def test_callback_action_settings_set_timezone_gmt(wind_bot, telegram_env, test_user, create_set_timezone_action,
                                                   database_fixture, caplog):
    mode = Strings.ActionTimezone.SET_TZ_MODE_GMT
    origin = 1234

    action_init = create_set_timezone_action(mode, None, origin)
    wind_bot.OnWebHook(action_init)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_GMT_TZ_INIT)

    action_next = create_set_timezone_action(mode, 'GMT+', origin)
    wind_bot.OnWebHook(action_next)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_GMT_TZ_NEXT.format(gmt="GMT+"))

    action_next = create_set_timezone_action(mode, 'GMT-', origin)
    wind_bot.OnWebHook(action_next)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_GMT_TZ_NEXT.format(gmt="GMT-"))

    action_confirm = create_set_timezone_action(mode, 'GMT+5', origin)
    wind_bot.OnWebHook(action_confirm)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_TZ_CONFIRM[:15])

    action_confirm = create_set_timezone_action(mode, 'GMT-10', origin)
    wind_bot.OnWebHook(action_confirm)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_TZ_CONFIRM[:15])

    action_confirm = create_set_timezone_action(mode, 'GMT-10', origin, True)
    wind_bot.OnWebHook(action_confirm)
    assert 'ERROR' not in caplog.text

    user = database_fixture.getUser(test_user.chat_id)
    assert user.timezone == 'GMT-10'


def test_callback_action_settings_set_timezone_hour(wind_bot, telegram_env, test_user, create_set_timezone_action,
                                                    database_fixture, caplog):
    mode = Strings.ActionTimezone.SET_TZ_MODE_HOUR
    origin = 1234

    action_init = create_set_timezone_action(mode, None, origin)
    wind_bot.OnWebHook(action_init)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_HOUR_TZ_INIT)

    action_next = create_set_timezone_action(mode, '0', origin)
    wind_bot.OnWebHook(action_next)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_HOUR_TZ_NEXT.format(now='0'))

    action_next = create_set_timezone_action(mode, '2', origin)
    wind_bot.OnWebHook(action_next)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_HOUR_TZ_NEXT.format(now='2'))

    action_next = create_set_timezone_action(mode, '22', origin)
    wind_bot.OnWebHook(action_next)
    assert 'ERROR' not in caplog.text

    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_TZ_CONFIRM.format(
        time=f'22:{datetime.now().minute:02d}'))

    action_next = create_set_timezone_action(mode, '01', origin)
    wind_bot.OnWebHook(action_next)
    assert 'ERROR' not in caplog.text
    assert_sent_message(Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_TZ_CONFIRM.format(
         time=f'01:{datetime.now().minute:02d}'))

    now = datetime.utcnow() + timedelta(hours=2)
    action_next = create_set_timezone_action(mode, f"{now.hour:02d}", origin, True)
    wind_bot.OnWebHook(action_next)
    assert 'ERROR' not in caplog.text

    user = database_fixture.getUser(test_user.chat_id)
    assert user.timezone == 'GMT+2'


def test_callback_action_settings_set_timezone_invalidMode(wind_bot, telegram_env, create_set_timezone_action, caplog):
    action_next = create_set_timezone_action('asdasdasd', 'asdas', 123, True)
    wind_bot.OnWebHook(action_next)
    assert 'ERROR' in caplog.text
    assert "confirmation received but mode:'asdasdasd' is invalid" in caplog.text
