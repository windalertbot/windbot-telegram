import pytest
import logging
import aws_lambda_factory


from typing import List

from datetime import datetime

from telebot.types import InlineKeyboardMarkup
from telegram.wind_bot import WindBot
from telegram.utils import TelegramApiMethods
from data.user import UserObjectProxy

from data.spot import SpotWeather, SpotInfo, SpotSearchResult


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


LAST_SENT_MESSAGE_ENV = 'LAST_SENT_MESSAGE'


class telegram_mock(WindBot):
    LAST_MARKUP = None

    def __init__(self, monkeypatch):
        super().__init__("YOU DONT NEED TOKEN")
        self.__monkeypatch = monkeypatch

    def send_message(self, chat_id, text, disable_web_page_preview=None, reply_to_message_id=None, reply_markup=None,
                     parse_mode=None, disable_notification=None, timeout=None):
        self.__monkeypatch.setenv(LAST_SENT_MESSAGE_ENV, text)
        telegram_mock.LAST_MARKUP = reply_markup
        self.__assert_inline_keyboard(reply_markup)
        logger.info(text)

    def edit_message_reply_markup(self, *args, **kwargs):
        assert False  # TODO: test it also

    def edit_message_text(self, text, chat_id, message_id, reply_markup=None, **kwargs):
        # TODO: check for ids
        self.__monkeypatch.setenv(LAST_SENT_MESSAGE_ENV, text)
        telegram_mock.LAST_MARKUP = reply_markup
        self.__assert_inline_keyboard(reply_markup)
        logger.info(text)

    def delete_message(self, chat_id, message_id, **kwargs):
        self.__monkeypatch.delenv(LAST_SENT_MESSAGE_ENV, raising=False)
        telegram_mock.LAST_MARKUP = None
        logger.info('Last message was deleted')

    def _createWebhookReply(self, api_action: str, **kwargs):
        if api_action == TelegramApiMethods.SEND_MESSAGE:
            self.send_message(**kwargs)
        elif api_action == TelegramApiMethods.EDIT_MESSAGE:
            self.edit_message_text(**kwargs)
        elif api_action == TelegramApiMethods.DELETE_MESSAGE:
            self.delete_message(**kwargs)

    def __assert_inline_keyboard(self, inline_keyboard):
        if isinstance(inline_keyboard, InlineKeyboardMarkup):
            for row in inline_keyboard.keyboard:
                for button in row:
                    assert len(button.callback_data) <= 64


class database_mock():
    __INSTANCE = None

    def __init__(self):
        super().__init__()
        self.init()

    @staticmethod
    def instance():
        if not database_mock.__INSTANCE:
            database_mock.__INSTANCE = database_mock()
        return database_mock.__INSTANCE

    def init(self):
        self.__users = []

    def getUsersToNotify(self) -> List[UserObjectProxy]:
        unix_utc_now = int(datetime.utcnow().timestamp())
        return list([i for i in self.__users if len(i.alerts) > 0 and i.snoozetime < unix_utc_now])

    def getUser(self, user_id) -> List[UserObjectProxy]:
        return next((i for i in self.__users if i['chat_id'] == user_id), None)

    def getUsersBatch(self, user_ids):
        return [u for u in self.__users if u.chat_id in user_ids]

    def updateUser(self, user):
        for index, u in enumerate(self.__users):
            if u.chat_id == user.chat_id:
                self.__users[index] = user
                return True
        return False

    def createUser(self, chat_id: int, timezone: str = None, **kwargs) -> None:
        self.__users.append(UserObjectProxy({
                "chat_id": chat_id,
                "snoozetime": 0,
                "timezone": timezone,
                "notify": [10, 18],
                'alerts': kwargs.get('alerts', [])
            }))

    def removeUser(self, chat_id: int):
        for indx, user in enumerate(self.__users):
            if user.chat_id == chat_id:
                self.__users.pop(indx)
                return


class windprovider_mock():
    def searchSpotByName(self, name: str) -> SpotSearchResult:
        if name.lower() == "none":
            return SpotSearchResult({'suggestions': {}})

        def __create_spot(n, c=None):
            return {'id': 'Id_' + n,
                    'n': n,
                    'c': 'Country_' + (c if c else n),
                    'kw': 'kw_' + n,
                    'lat': 0,
                    'lon': 1
                    }

        size = 1 if name.startswith("single") else 2
        country = "same" if name.lower() == "same country" else None

        spots = []
        for s in range(0, size):
            spots.append(__create_spot(name if s == 0 else name + str(s), country))

        return SpotSearchResult({
            'suggestions': {
                'spot': spots
            }
        })

    def getSpotInfo(self, spot: str) -> SpotInfo:
        if spot.lower() == "none":
            return None

        return SpotInfo({
            'id': spot,
            'n': 'Name_' + spot,
            'c': 'Country_' + spot,
            'kw': 'kw_' + spot,
            'lat': 0,
            'lon': 1
        })

    def getCurrentState(self, spot_ids):
        return [
            SpotWeather({
                'id': spot_id,
                'dtl': datetime(year=2020, month=8, day=1, hour=15).isoformat(),
                'wd': 180,
                'ws': 50,
                'wg': 44,
                'cl': 0.1,
                'at': 44,
                'ap': 1245
            }) for spot_id in spot_ids
        ]


@pytest.fixture(scope='session')
def session_mocked_databse():
    return database_mock.instance()


@pytest.fixture(scope='function')
def with_patched_telegram(monkeypatch):
    original = aws_lambda_factory.Factory.AccessWindbot
    mocked_telegram = telegram_mock(monkeypatch)

    def _get_mocked(*args, **kwargs):
        return mocked_telegram

    assert type(original) is not _get_mocked
    aws_lambda_factory.Factory.AccessWindbot = _get_mocked
    yield
    aws_lambda_factory.Factory.AccessWindbot = original


@pytest.fixture(scope='function')
def with_patched_database(session_mocked_databse):
    session_mocked_databse.init()

    def _createDatabase(*args, **kwargs):
        return session_mocked_databse

    original = aws_lambda_factory.Factory.AccessDatabase
    assert type(original) is not _createDatabase

    aws_lambda_factory.Factory.AccessDatabase = _createDatabase
    yield
    aws_lambda_factory.Factory.AccessDatabase = original


@pytest.fixture(scope='function')
def with_patched_windprovider():
    mocked_windprovider = windprovider_mock()

    def _get_mocked(*args, **kwargs):
        return mocked_windprovider

    original = aws_lambda_factory.Factory.AccessWindprovider
    assert type(original) is not _get_mocked
    aws_lambda_factory.Factory.AccessWindprovider = _get_mocked
    yield
    aws_lambda_factory.Factory.AccessWindprovider = original


@pytest.fixture(scope='function')
def with_patched_factory(with_patched_telegram, with_patched_database, with_patched_windprovider):
    pass


@pytest.fixture
def get_database():
    def _get_database():
        return aws_lambda_factory.Factory.AccessDatabase("http://localhost:8000")
    return _get_database


@pytest.fixture
def database_fixture(get_database):
    return get_database()


@pytest.fixture
def windprovider_fixture():
    return aws_lambda_factory.Factory.AccessWindprovider()


@pytest.fixture
def wind_bot(with_patched_telegram):
    return aws_lambda_factory.Factory.AccessWindbot()
