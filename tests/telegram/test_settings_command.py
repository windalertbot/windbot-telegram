from datetime import datetime

from tests.telegram.conftest import assert_sent_message


def test_windbot_command_settings_get(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/settings"))
    assert_sent_message('Your settings:')


def test_windbot_command_settings_set_tz(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/settings timezone=GMT+8"))
    assert_sent_message('Timezone updated')

    # check
    wind_bot.OnWebHook(create_webhook_message("/settings"))
    assert_sent_message('GMT+8')

    # Set valid from hour
    now_gmt = (datetime.utcnow().hour + 1) % 24
    wind_bot.OnWebHook(create_webhook_message("/settings timezone={0}".format(now_gmt)))
    assert_sent_message('Timezone updated')

    # check
    wind_bot.OnWebHook(create_webhook_message("/settings"))
    assert_sent_message('GMT+1')

    # Set valid from hour
    now_gmt = (datetime.utcnow().hour - 6) % 24
    wind_bot.OnWebHook(create_webhook_message("/settings timezone={0}".format(now_gmt)))
    assert_sent_message('Timezone updated')

    # check
    wind_bot.OnWebHook(create_webhook_message("/settings"))
    assert_sent_message('GMT-6')


def test_windbot_command_settings_set_tz_invalid(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/settings timezone=asdasd"))
    assert_sent_message("Value:'asdasd' is invalid")

    wind_bot.OnWebHook(create_webhook_message("/settings timezone=44"))
    assert_sent_message("Hour value: '44' is invalid")


def test_windbot_command_settings_set_notify_time(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/settings notify=[5,22]"))
    assert_sent_message('Notification time updated')
    # check
    wind_bot.OnWebHook(create_webhook_message("/settings"))
    assert_sent_message('from 5:00 till 22:00')

    # set with quotes and wrong order
    wind_bot.OnWebHook(create_webhook_message("/settings notify='[23, 10]'"))
    assert_sent_message('Notification time updated')
    # check
    wind_bot.OnWebHook(create_webhook_message("/settings"))
    assert_sent_message('from 10:00 till 23:00')


def test_windbot_command_settings_set_notify_time_invalid(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/settings notify=asdasd"))
    assert_sent_message("Value:'asdasd' has invalid")

    wind_bot.OnWebHook(create_webhook_message("/settings notify=[5, 11]"))
    assert_sent_message("Value:'[5,' has invalid")

    wind_bot.OnWebHook(create_webhook_message("/settings notify=[44,5]"))
    assert_sent_message("Invalid morning value: '44'")

    wind_bot.OnWebHook(create_webhook_message("/settings notify=[10,35]"))
    assert_sent_message("Invalid evening value: '35'")


def test_windbot_command_settings_set_invalid(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/settings asdasdas"))
    assert_sent_message('Unrecognized arguments:')


def test_windbot_command_settings_set_invalid_key(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/settings ololo=xxx"))
    assert_sent_message("Unknown key:'ololo'")


def test_windbot_command_settings_user_not_found(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/settings", 98764))
    assert_sent_message("You wasn't found in database")

    wind_bot.OnWebHook(create_webhook_message("/settings timezone=22", 98764))
    assert_sent_message("You wasn't found in database")

    wind_bot.OnWebHook(create_webhook_message("/settings notify=[0,1]", 98764))
    assert_sent_message("You wasn't found in database")
