import pytest
import os

from tests.telegram.conftest import assert_sent_message
from data.alert import AlertObjectProxy
from data.spot import SpotWeather
from tests.conftest import LAST_SENT_MESSAGE_ENV


@pytest.fixture
def create_user(database_fixture):
    def _create_user(id):
        database_fixture.createUser(id)

    return _create_user


@pytest.fixture
def create_notification():
    def _create_notification(user_id, spots):
        def __create_spot_with_data(id):
            spot = AlertObjectProxy({
                'id': id,
                'desired_wind_speed': 15,
                'desired_wind_direction': [180, 330],
                'name': f"Name_{id}"
                })
            spot.weather_data = SpotWeather({
                'id': id,
                'dtl': "0",
                'wd': 180,
                'ws': 50
            })
            return spot
        return user_id, [__create_spot_with_data(i) for i in spots]
    return _create_notification


def test_send_notifications(wind_bot, telegram_env, create_user, create_notification, monkeypatch):
    notifications = {}
    user_ids = [i for i in range(0, 2)]

    for id in user_ids:
        create_user(id)
        k, v = create_notification(id, ['xx', 'zz'])
        notifications[k] = v

    wind_bot.SendNotifications(notifications)
    assert_sent_message('There is wind in Name_zz!')

    monkeypatch.delenv(LAST_SENT_MESSAGE_ENV)
    assert os.getenv(LAST_SENT_MESSAGE_ENV) is None, "Env wasn't cleared"

    # THIS IS OK SCENARIO. Bot only sends notification and set snooze time.
    # Bot doesn't check user snooze time is valid - this is checked in lambda handler
    wind_bot.SendNotifications(notifications)
    assert_sent_message('There is wind in Name_zz!')
