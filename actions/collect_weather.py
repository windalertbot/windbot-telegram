import logging
import math

from typing import List, Dict

from datetime import datetime
from dateutil import tz

from interfaces import IWindProvider

from data.user import UserObjectProxy
from data.spot import SpotWeather

logger = logging.getLogger(__name__)


class WeatherCollector():

    """
        Do api calls, and collect weather data for unique spots in users' alerts
    """
    @staticmethod
    def collect(wind_provider: IWindProvider, users: List[UserObjectProxy]) -> Dict[str, SpotWeather]:
        result = {}

        all_spot_ids = []
        for user in users:
            # not need to check spots for sleeping users
            if user.timezone:
                now = datetime.now(tz.gettz(user.timezone))
                if now.hour < user.awake or now.hour > user.sleep:
                    continue

            all_spot_ids.extend([alert.spot_id for alert in user.alerts])

        # make uniqie list
        unique_spot_ids = list(set(all_spot_ids))
        logger.debug("Unique spots for update (count:{0})\n{1}".format(len(unique_spot_ids), unique_spot_ids))

        # do batch requests to API
        batch_size = 50  # 50 is enough I guess. (however we need to figure out the limit)

        for subset in [unique_spot_ids[i:i + batch_size] for i in range(0, len(unique_spot_ids), batch_size)]:
            data = wind_provider.getCurrentState(subset)
            for item in data:
                result[item.id] = item

        logger.debug("API was called {0} times".format(math.ceil(len(unique_spot_ids) / batch_size)))
        return result
