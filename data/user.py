from typing import List, Union

from datetime import datetime, timedelta
from dateutil import tz
from data.common import JsonObjectProxy
from data.alert import AlertObjectProxy


class UserObjectProxy(JsonObjectProxy):
    ALERTS_LIMIT = 5

    def __init__(self, json):
        super().__init__(json)
        self.__updateAlerts()

    @property
    def chat_id(self) -> int:
        return self['chat_id']

    @property
    def timezone(self) -> str:
        return self['timezone']

    @property
    def awake(self) -> int:
        """
        Time when user awakes
        """
        return self['notify'][0]

    @property
    def sleep(self) -> int:
        """
        Time when user start sleeping
        """
        return self['notify'][1]

    @property
    def snoozetime(self) -> List[int]:
        return self['snoozetime']

    @property
    def __spots(self) -> List[AlertObjectProxy]:
        return self.__alerts

    @property
    def alerts(self) -> List[AlertObjectProxy]:
        return self.__alerts

    def snooze(self) -> None:
        timezone = self.timezone if self.timezone else 'GMT'

        snooze_until = datetime.now(tz.gettz(timezone)) + timedelta(days=1)
        snooze_until = snooze_until.replace(hour=int(self.awake), minute=0, second=0)
        self['snoozetime'] = int(snooze_until.timestamp())

    def addAlert(self, new_alert: AlertObjectProxy) -> None:
        for spot in self['alerts']:
            if spot['id'] == new_alert.spot_id:
                for key in spot:
                    spot[key] = new_alert[key]

                self.__updateAlerts()
                return

        self['alerts'].append(new_alert._json)
        self.__updateAlerts()

    def delAlert(self, id: str) -> AlertObjectProxy:
        for index, spot in enumerate(self['alerts']):
            if spot['id'] == id:
                removed = self['alerts'].pop(index)
                self.__updateAlerts()
                return removed
        return None

    def getAlert(self, id: str) -> Union[AlertObjectProxy, None]:
        for alert in self.alerts:
            if alert.spot_id == id:
                return alert
        return None

    def __updateAlerts(self) -> None:

        # "migration"
        if self['spots']:
            self['alerts'] = self['spots']

        self.__alerts = list([AlertObjectProxy(i) for i in self['alerts']])
