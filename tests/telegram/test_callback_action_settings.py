from telegram.callback_actions import Strings
from tests.telegram.conftest import assert_sent_message


def test_callback_action_settings_edit(wind_bot, telegram_env, create_callback_action_message, caplog):
    action = create_callback_action_message(Strings.ActionSettings.NAME, {
        Strings.ActionSettings.DATA_DO_KEY: None,
        Strings.ActionBase.DATA_ORIGIN_NAME: 1234
        })

    wind_bot.OnWebHook(action)
    assert 'ERROR' not in caplog.text

    action = create_callback_action_message(Strings.ActionSettings.NAME, {
        Strings.ActionSettings.DATA_DO_KEY: Strings.ActionSettings.DATA_EDIT_NAME,
        Strings.ActionBase.DATA_ORIGIN_NAME: 1234
        })
    wind_bot.OnWebHook(action)
    assert_sent_message(Strings.ActionSettings.USER_SETTINGS_CALLBACK_SELECT_EDIT_OPTION)
