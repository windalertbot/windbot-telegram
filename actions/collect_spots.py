import logging

from typing import List, Dict, Tuple

from data.spot import SpotWeather
from data.user import UserObjectProxy
from data.alert import AlertObjectProxy

from datetime import datetime
from dateutil import tz

logger = logging.getLogger(__name__)


class UserNotificationCollector():

    @staticmethod
    def collect(users: List[UserObjectProxy],
                all_weather_data: Dict[str, SpotWeather]) -> Dict[int, List[AlertObjectProxy]]:

        result = {}

        for user in users:
            try:
                id, spots = UserNotificationCollector.__processUser(user, all_weather_data)
                if id and spots:
                    result[id] = spots

                logger.debug("User was {0} processed".format(user.chat_id))
            except Exception as Error:  # pragma: no cover
                logger.error("Exception happened whilst processing user:{0}.\n{1}".format(user.chat_id, Error))
                continue

        return result

    @staticmethod
    def __processUser(user: UserObjectProxy, all_weather_data: Dict[str, SpotWeather]) -> Tuple[int, AlertObjectProxy]:
        # the `user.timezone` value could be any trash
        user_tz = tz.gettz(user.timezone) if user.timezone else None

        # check if user sleeping now
        if user_tz:
            now = datetime.now(user_tz)
            if now.hour < user.awake or now.hour > user.sleep:
                return None, None

        # get triggered spots
        spots = UserNotificationCollector.__collectSpotsForUserNotification(all_weather_data, user.alerts)

        filtered_spots = []
        if not user_tz:
            for spot in spots:
                now = spot.weather_data.date
                if now.hour < user.awake or now.hour > user.sleep:
                    filtered_spots.append(spot)
            spots = [s for s in spots if s not in filtered_spots]

        if len(spots) > 0:
            return user.chat_id, spots

        return None, None

    @staticmethod
    def __collectSpotsForUserNotification(all_weather_data: Dict[str, SpotWeather],
                                          user_alerts: List[AlertObjectProxy]) -> List[AlertObjectProxy]:

        triggered_alerts = []

        # check if any spot has desired conditions
        for alert in user_alerts:
            if alert.spot_id not in all_weather_data:
                logger.warning(f"Spot id:{alert.spot_id} was not found in actual weather data.")
                continue

            weather_data = all_weather_data[alert.spot_id]

            # check speed
            if weather_data.wind_speed < alert.desired_wind_speed:
                continue

            # check direction
            start = alert.desired_wind_direction[0]
            end = alert.desired_wind_direction[1]

            wdir = weather_data.wind_direction
            if end > start:
                if wdir < start or wdir > end:
                    continue
            else:
                if wdir > end and wdir < start:
                    continue

            # all checks done - spot fit
            alert.weather_data = weather_data
            triggered_alerts.append(alert)

        return triggered_alerts
