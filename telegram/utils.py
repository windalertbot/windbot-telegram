from telebot.apihelper import _convert_markup


class TelegramApiMethods:
    SEND_MESSAGE = 'sendMessage'
    EDIT_MESSAGE = 'editMessageText'
    EDIT_MESSAGE_MARKUP = 'editMessageReplyMarkup'
    DELETE_MESSAGE = 'deleteMessage'


class TelegramMessageFields:
    """
        Reduce set of available fields from:
            https://core.telegram.org/bots/api#message
    """
    TEXT = 'text'
    CAPTION = 'caption'
    CHAT_ID = 'chat_id'
    MESSAGE_ID = 'message_id'
    INLINE_MESSAGE_ID = 'inline_message_id'
    PARSE_MODE = 'parse_mode'
    DISABLE_WEB_PAGE_PREVIEW = 'disable_web_page_preview'
    REPLY_MARKUP = 'reply_markup'
    CONNECT_TIMEOUT = 'connect-timeout'


def create_message_payload(**kwargs):
    payload = {}

    fieldsClass = TelegramMessageFields
    fields = [fieldsClass.__getattribute__(fieldsClass, f) for f in dir(fieldsClass) if not f.startswith('_')]

    for field in fields:
        if field in kwargs and kwargs[field] is not None:
            if field == TelegramMessageFields.REPLY_MARKUP:
                payload[field] = _convert_markup(kwargs[field])
            else:
                payload[field] = kwargs[field]

    return payload
