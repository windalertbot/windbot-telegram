import json
import logging
import actions.user_actions
import telegram.message_utils as message_utils

from typing import Union, Dict, List, Any

from telebot.types import Message, InlineKeyboardMarkup, InlineKeyboardButton

from telegram.callback_actions import Strings
from interfaces import IWindbot

logger = logging.getLogger(__name__)


class BaseAction(object):

    def __init__(self, message: Message, data):
        super().__init__()
        self.data = data
        self.message = message

        self.__origin = self.data.get(Strings.ActionBase.DATA_ORIGIN_NAME, None)
        self.__message_id = message.id
        self.__chat_id = message.chat.id
        self.__user = None

    @property
    def origin(self) -> Union[int, None]:
        return self.__origin

    @property
    def messageId(self) -> Union[int, None]:
        return self.__message_id

    @property
    def chatId(self) -> Union[int, None]:
        return self.__chat_id

    @property
    def user(self) -> Union[int, None]:
        if not self.__user:
            self.__user = actions.user_actions.UserActions.GetUser(self.chatId)
        return self.__user

    def process(self, telegram: IWindbot, **kwargs) -> None:
        raise NotImplementedError("Should be implemented in derived")

    def _cleanup(self, telegram: IWindbot, with_origin=True):
        # first delete origin message (as reply if supported)
        if with_origin and self.origin:
            telegram.DeleteMessage(self.chatId, self.origin)

        telegram.DeleteMessage(self.chatId, self.messageId)

    def _show_settings(self, telegram: IWindbot, text: str = None):
        if not self.user:
            self._cleanup(telegram)
        else:
            telegram.UpdateMessage(
                self.message,
                message_utils.create_settings_message_reply(self.user, self.origin, text))

    class Markup:
        @staticmethod
        def build(text: str, keyboard: InlineKeyboardMarkup = None, parse: str = 'Markdown'):
            return {
                'parse_mode': parse,
                'reply_markup': keyboard,
                'text': text
            }

        @staticmethod
        def num_to_emoji(num: Union[int, str]):
            try:
                if isinstance(num, int):
                    if num < len(Strings.Keyboard.NUMBERS_EMOJI):
                        return Strings.Keyboard.NUMBERS_EMOJI[num]
                    else:
                        num = str(num)
                return ''.join([Strings.Keyboard.NUMBERS_EMOJI[int(symbol)] for symbol in num])

            except Exception as e:
                logger.error(f"Cannot covert:'{num}' to emoji string.\n{e}")
                return '❌❌'

        @staticmethod
        def build_numeric_keyboard(action: str, create_action_arguments, button_prefix=None, min_number=0,
                                   row_width=5, create_button_text=None, max_number=10):

            def _create_button_text(button):
                if create_button_text:
                    return create_button_text(button)
                else:
                    button_text = BaseAction.Markup.num_to_emoji(button)
                    if button_prefix:
                        button_text = button_prefix + button_text
                    return button_text

            return BaseAction.Markup.build_keyboard(
                list(range(min_number, max_number)),
                action,
                create_action_arguments,
                row_width=row_width,
                create_button_text=_create_button_text
            )

        @staticmethod
        def build_yes_no_keyboard(action: str, yes_args: Dict[str, str], no_args: Dict[str, str]):
            markup = InlineKeyboardMarkup()
            markup.row(
                BaseAction.Markup.create_button(Strings.Keyboard.BUTTON_NO, action, no_args),
                BaseAction.Markup.create_button(Strings.Keyboard.BUTTON_YES, action, yes_args))
            return markup

        @staticmethod
        def build_keyboard(buttons: List[Any], action: str, create_action_arguments,
                           row_width=5, create_button_text=lambda x: str(x)):
            markup = InlineKeyboardMarkup()

            row = []
            for button in buttons:
                row.append(
                    BaseAction.Markup.create_button(
                        create_button_text(button),
                        action,
                        create_action_arguments(button)
                        ))

                if len(row) >= row_width:
                    markup.row(*row)
                    row.clear()

            if row:
                markup.row(*row)

            return markup

        @staticmethod
        def back_button(action: str, arguments: Dict[str, str]):
            return BaseAction.Markup.create_button(Strings.Keyboard.BUTTON_BACK, action, arguments)

        @staticmethod
        def close_button(action: str, arguments: Dict[str, str]):
            return BaseAction.Markup.create_button(Strings.Keyboard.BUTTON_CLOSE, action, arguments)

        @staticmethod
        def cancel_button(action: str, arguments: Dict[str, str]):
            return BaseAction.Markup.create_button(Strings.Keyboard.BUTTON_CANCEL, action, arguments)

        @staticmethod
        def create_button(caption: str, action: str, arguments: Dict[str, str]):
            return InlineKeyboardButton(
                text=caption,
                callback_data=BaseAction.CallbackData.Serialize(action, arguments)
            )

    class CallbackData:

        @staticmethod
        def Serialize(action: str, data: Dict[str, Any]) -> str:
            ser = data.copy()
            if Strings.ActionsFactory.ACTION_FIELD_NAME in ser:
                raise KeyError(f"Invalid key '{Strings.ActionsFactory.ACTION_FIELD_NAME}' in callback data")
            ser[Strings.ActionsFactory.ACTION_FIELD_NAME] = action

            return json.dumps(ser, separators=(',', ':'))[1:-1]

        @staticmethod
        def Deserialize(data_string: str) -> Dict[str, Any]:
            return json.loads(f"{{{data_string}}}")
