from data.alert import AlertObjectProxy

from actions.user_actions import UserActions
from tests.telegram.conftest import assert_sent_message
from telegram.callback_actions import Strings


def test_callback_action_remove_spot_no_spotid(wind_bot, telegram_env, create_callback_action_message,
                                               create_webhook_message, caplog):
    # /remove command without arguments will show interactive menu
    # which should be deleted
    wind_bot.OnWebHook(create_webhook_message("/remove"))

    action = create_callback_action_message(Strings.ActionRemoveAlert.NAME, {
        Strings.ActionRemoveAlert.DATA_SPOT_ID_NAME: None,
        Strings.ActionBase.DATA_ORIGIN_NAME: 1234
        })

    wind_bot.OnWebHook(action)
    assert_sent_message(Strings.ActionRemoveAlert.NO_SPOTS_TO_REMOVE)


def test_callback_action_remove_spot_cancel(wind_bot, telegram_env, create_callback_action_message,
                                            create_webhook_message, caplog, assert_last_message_deleted):

    wind_bot.OnWebHook(create_webhook_message("/remove"))

    action = create_callback_action_message(Strings.ActionRemoveAlert.NAME, {
        Strings.ActionRemoveAlert.DATA_CANCEL: True,
        Strings.ActionBase.DATA_ORIGIN_NAME: 1234
        })

    wind_bot.OnWebHook(action)
    assert_last_message_deleted()


def test_callback_action_remove_spot(wind_bot, telegram_env, create_callback_action_message, test_user,
                                     database_fixture, caplog, assert_markup_button):

    assert len(test_user.alerts) == 0

    test_user.addAlert(AlertObjectProxy({
        'id': 'spot1',
        'name': 'spot name 1',
        'ws': 1,
        'wd': [0-359]
    }))
    assert database_fixture.updateUser(test_user)

    updated_user = database_fixture.getUser(test_user.chat_id)
    assert len(updated_user.alerts) == 1

    action = create_callback_action_message(Strings.ActionRemoveAlert.NAME, {
        Strings.ActionRemoveAlert.DATA_SPOT_ID_NAME: 'spot1',
        })
    wind_bot.OnWebHook(action)
    assert_markup_button(Strings.Keyboard.BUTTON_YES)
    assert_sent_message('Alert was removed', False)

    action = create_callback_action_message(Strings.ActionRemoveAlert.NAME, {
        Strings.ActionRemoveAlert.DATA_SPOT_ID_NAME: 'spot1',
        Strings.ActionRemoveAlert.DATA_CONFIRM: True,
        })
    wind_bot.OnWebHook(action)

    updated_user = database_fixture.getUser(test_user.chat_id)
    assert len(updated_user.alerts) == 0

    assert 'ERROR' not in caplog.text
    assert_sent_message('Alert was removed')


def test_callback_action_remove_no_arg(wind_bot, telegram_env, create_callback_action_message, test_user,
                                       database_fixture, caplog, assert_markup_button):
    assert len(test_user.alerts) == 0

    test_user.addAlert(AlertObjectProxy({
        'id': 'spotXXX',
        'name': 'spot name XXX',
        'ws': 1,
        'wd': [0-359]
    }))
    assert database_fixture.updateUser(test_user)

    updated_user = database_fixture.getUser(test_user.chat_id)
    assert len(updated_user.alerts) == 1

    action = create_callback_action_message(Strings.ActionRemoveAlert.NAME, {
        Strings.ActionRemoveAlert.DATA_SPOT_ID_NAME: None,
        })
    wind_bot.OnWebHook(action)
    assert_markup_button('spotXXX')
    assert_markup_button(Strings.Keyboard.BUTTON_CLOSE)


def test_callback_action_remove_no_user(wind_bot, telegram_env, create_callback_action_message, test_user,
                                        database_fixture, caplog):
    action = create_callback_action_message(Strings.ActionRemoveAlert.NAME, {
        Strings.ActionRemoveAlert.DATA_SPOT_ID_NAME: None,
        },
        chat_id=7987978)
    wind_bot.OnWebHook(action)
    assert_sent_message(UserActions.Strings.USER_NOT_FOUND_MESSAGE)
