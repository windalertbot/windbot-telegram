from typing import Tuple, List
from datetime import datetime

from data.common import JsonObjectProxy


class SpotWeather(JsonObjectProxy):
    def __init__(self, json):
        super().__init__(json)

    @property
    def id(self) -> str:
        """
        AlertObjectProxy id
        """
        return self['id']

    @property
    def date(self) -> datetime:
        """
        AlertObjectProxy date
        """
        return datetime.fromisoformat(self['dtl'])

    @property
    def wind_direction(self) -> int:
        """
        Wind direction in degrees
        """
        return self['wd']

    @property
    def wind_speed(self) -> int:
        """
        Wind speed in knots
        """
        return self['ws']

    @property
    def wind_gusts(self) -> int:
        """
        Wind speed in knots
        """
        return self['wg']

    @property
    def cloud_cover(self) -> float:
        return self['cl']

    @property
    def air_temperature(self) -> float:
        return self['at']

    @property
    def air_pressure(self) -> int:
        return self['ap']

    @property
    def uknown_p(self):
        return self['p']

    @property
    def uknown_tp(self):
        return self['tp']


class SpotInfo(JsonObjectProxy):
    _UNKNOWN_ATTRIBUTES = ["c_at", "el", "has", "is_geo", "is_ts", "is_was", "is_ws", "o_id", "r_id", "u_at", "v"]

    def __init__(self, json):
        super().__init__(json)

    @property
    def id(self) -> str:
        return self['id']

    @property
    def name(self) -> str:
        return self['n']

    @property
    def country(self):
        return self['c']

    @property
    def country_id(self):
        return self['c_id']

    @property
    def keyword(self) -> str:
        return self['kw']

    @property
    def coordinates(self) -> Tuple[int, int]:
        return (self['lat'], self['lon'])


class SpotSearchResultItem(JsonObjectProxy):
    def __init__(self, json):
        super().__init__(json)

    @property
    def id(self) -> str:
        return self['id']

    @property
    def name(self) -> str:
        return self['n']

    @property
    def country(self):
        return self['c']

    @property
    def keyword(self) -> str:
        return self['kw']

    @property
    def coordinates(self) -> Tuple[int, int]:
        return (self['lat'], self['lon'])


class SpotSearchResult():
    def __init__(self, json):
        self._spots = []
        suggestions = json["suggestions"]

        if 'spot' in suggestions:
            for spot in suggestions["spot"]:
                self.spots.append(SpotSearchResultItem(spot))

    @property
    def spots(self) -> List[SpotSearchResultItem]:
        return self._spots
