from tests.telegram.conftest import assert_sent_message
from telegram.callback_actions import Strings


def test_callback_action_find_spot_wrong_data(wind_bot, telegram_env, create_callback_action_message, caplog):
    action = create_callback_action_message(Strings.ActionFindAlert.NAME, {
        Strings.ActionFindAlert.DATA_SEARCH_STR: None,
        })

    wind_bot.OnWebHook(action)
    assert 'invalid search string' in caplog.text
    caplog.clear()

    action = create_callback_action_message(Strings.ActionFindAlert.NAME, {
        Strings.ActionFindAlert.DATA_SEARCH_STR: None,
        Strings.ActionFindAlert.DATA_CANCEL: 'true',
        })

    wind_bot.OnWebHook(action)
    assert 'invalid search string' not in caplog.text
    caplog.clear()

    action = create_callback_action_message(Strings.ActionFindAlert.NAME, {
        Strings.ActionFindAlert.DATA_SEARCH_STR: 'XX',
        })

    wind_bot.OnWebHook(action)
    assert "invalid search string:'XX'" in caplog.text
    caplog.clear()


def test_callback_action_find_spot(wind_bot, telegram_env, create_callback_action_message, caplog,
                                   assert_markup_button):
    action = create_callback_action_message(Strings.ActionFindAlert.NAME, {
        Strings.ActionFindAlert.DATA_SEARCH_STR: 'hello',
        })

    wind_bot.OnWebHook(action)
    assert_sent_message(Strings.ActionFindAlert.SELECT_COUNTRY_REPLY_TEXT.format(search='hello'))
    assert_markup_button('Country_hello')
    assert_markup_button('asdasdasd', False)

    action = create_callback_action_message(Strings.ActionFindAlert.NAME, {
        Strings.ActionFindAlert.DATA_SEARCH_STR: 'qqqq',
        Strings.ActionFindAlert.DATA_COUNTRY_CODE: 'Country_qqqq',
        })

    wind_bot.OnWebHook(action)
    assert_sent_message(Strings.ActionFindAlert.SELECT_SPOT_REPLY_TEXT.format(search='qqqq', country='Country_qqqq'))
    assert_markup_button('Id_qqqq')
