import utils
from tests.telegram.conftest import assert_sent_message
from utils import parseWindDir
from telegram.callback_actions import Strings


def test_callback_action_add_spot_wrong_data(wind_bot, telegram_env, create_callback_action_message, caplog):
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: None,
        })

    wind_bot.OnWebHook(action)
    assert 'was triggered without spot id' in caplog.text
    caplog.clear()

    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: None,
        Strings.ActionAddAlert.DATA_CANCEL: 'true',
        })

    wind_bot.OnWebHook(action)
    assert 'was triggered without spot id' not in caplog.text


def test_callback_action_add_spot_set_wind_speed(wind_bot, telegram_env, create_callback_action_message,
                                                 assert_markup_button):
    spotId = 'SpotId'

    # clicked add
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spotId,
        })
    wind_bot.OnWebHook(action)

    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_WINDSPEED_TEXT.format(
        id=spotId,
        speed=''
    ))
    assert_markup_button(Strings.Keyboard.BUTTON_CANCEL)
    assert_markup_button(Strings.Keyboard.NUMBERS_EMOJI[5])

    # clicked on 1
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spotId,
        Strings.ActionAddAlert.DATA_WIND_SPEED: '1',
        })
    wind_bot.OnWebHook(action)

    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_WINDSPEED_TEXT.format(
        id=spotId,
        speed='1'
    ))
    assert_markup_button(Strings.Keyboard.BUTTON_BACK)

    # clicked on 2
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spotId,
        Strings.ActionAddAlert.DATA_WIND_SPEED: '12',
        })
    wind_bot.OnWebHook(action)

    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_WINDSPEED_CONFIRM.format(
        id=spotId,
        speed='12'
    ))
    assert_markup_button(Strings.Keyboard.BUTTON_YES)
    assert_markup_button(Strings.Keyboard.NUMBERS_EMOJI[2], False)

    # clicked on yes
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spotId,
        Strings.ActionAddAlert.DATA_WIND_SPEED: '12',
        Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
        })
    wind_bot.OnWebHook(action)
    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_WINDDIR_TEXT_START.format(
        id=spotId,
        speed='12'
    ))


def test_callback_action_add_spot_set_wind_dir(wind_bot, telegram_env, create_callback_action_message,
                                               assert_markup_button, assert_markup_callback_data):
    spotId = 'SpotId'
    wspeed = '44'

    # Speed confirmed
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spotId,
        Strings.ActionAddAlert.DATA_WIND_SPEED: wspeed,
        Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
        })
    wind_bot.OnWebHook(action)

    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_WINDDIR_TEXT_START.format(
        id=spotId,
        speed=wspeed
    ))
    assert_markup_button(Strings.Keyboard.BUTTON_BACK)
    assert_markup_button('SW')
    assert_markup_button('🧭')
    assert_markup_callback_data('NE')
    assert_markup_callback_data('🧭', False)

    # first direction N selected
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spotId,
        Strings.ActionAddAlert.DATA_WIND_SPEED: wspeed,
        Strings.ActionAddAlert.DATA_WIND_DIR: 'N',
        Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
        })
    wind_bot.OnWebHook(action)

    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_WINDDIR_TEXT_END.format(
        id=spotId,
        speed=wspeed,
        wdir=utils.getWindDirEmoji('N')
    ))
    assert_markup_button(Strings.Keyboard.BUTTON_BACK)
    assert_markup_button('NW')
    assert_markup_button('🧭')
    assert_markup_callback_data('N-E')
    assert_markup_callback_data('N-W')
    assert_markup_callback_data('N-SE')

    # second direction N-SW selected
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spotId,
        Strings.ActionAddAlert.DATA_WIND_SPEED: wspeed,
        Strings.ActionAddAlert.DATA_WIND_DIR: 'N-SE',
        Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
        })
    wind_bot.OnWebHook(action)

    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_WINDDIR_TEXT_CONFIRM.format(
        id=spotId,
        speed=wspeed,
        wdir='N-SE',
        wdir_map=utils.getWindDirEmoji('N-SE')
    ))
    assert_markup_button('NW', False)
    assert_markup_button(Strings.Keyboard.BUTTON_YES)


def test_callback_action_add_spot_set_alert_confirm(wind_bot, telegram_env, create_callback_action_message,
                                                    assert_markup_button, assert_markup_callback_data):
    spotId = 'ss1'
    wspeed = '07'
    wdir = 'SW-NE'

    # Speed confirmed
    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spotId,
        Strings.ActionAddAlert.DATA_WIND_SPEED: wspeed,
        Strings.ActionAddAlert.DATA_WIND_DIR: wdir,
        Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_DIRECTION,
        })
    wind_bot.OnWebHook(action)

    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_ALERT_TEXT_CONFIRM.format(
        id=spotId,
        speed=wspeed,
        wdir=wdir,
        wdir_map=utils.getWindDirEmoji(wdir)
    ))
    assert_markup_button(Strings.Keyboard.BUTTON_YES)
    assert_markup_callback_data(Strings.ActionAddAlert.CONFIRMED_ALERT)


def test_callback_action_add_spot_set_alert_correct(wind_bot, telegram_env, create_callback_action_message,
                                                    assert_markup_button, assert_markup_callback_data,
                                                    database_fixture):

    chat_id = 9678
    database_fixture.createUser(chat_id)

    spot_id = 'zxca'
    wspeed = 27
    wdir = 'SW-NE'
    wdir_deg = parseWindDir(wdir)

    user = database_fixture.getUser(chat_id)
    assert len(user.alerts) == 0

    action = create_callback_action_message(Strings.ActionAddAlert.NAME, {
        Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
        Strings.ActionAddAlert.DATA_WIND_SPEED: wspeed,
        Strings.ActionAddAlert.DATA_WIND_DIR: wdir,
        Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_ALERT,
        }, chat_id=chat_id)
    wind_bot.OnWebHook(action)

    user = database_fixture.getUser(chat_id)

    assert len(user.alerts) == 1
    assert user.alerts[0].spot_id == spot_id
    assert user.alerts[0].desired_wind_speed == wspeed
    assert user.alerts[0].desired_wind_direction[0] == wdir_deg[0]
    assert user.alerts[0].desired_wind_direction[1] == wdir_deg[1]
