from data.common import JsonObjectProxy


class LambdaEventAction():
    # A telgram server webhook - some user send us something
    WEBHOOK = 'webhook'

    # A cloudwatch event - once in period we need to check data
    UPDATE = 'update'


class LambdaEvent(JsonObjectProxy):
    def __init__(self, _event):
        super().__init__(_event)

    @property
    def action(self) -> str:
        return self['action']

    @property
    def token(self) -> str:
        return self['token']

    @property
    def payload(self) -> str:
        return self['payload']
