import logging

from typing import Dict, Any

from telebot.types import InlineKeyboardMarkup

from interfaces import IWindbot
from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction

from actions.user_actions import UserActions
from data.user import UserObjectProxy


logger = logging.getLogger(__name__)


class RemoveAlertAction(BaseAction):
    def __init__(self, message, data):
        super().__init__(message, data)
        self.spot = data.get(Strings.ActionRemoveAlert.DATA_SPOT_ID_NAME, None)
        self.cancel = data.get(Strings.ActionRemoveAlert.DATA_CANCEL, None)
        self.confirmed = data.get(Strings.ActionRemoveAlert.DATA_CONFIRM, None)

    def process(self, telegram: IWindbot, **kwargs) -> None:
        if self.cancel:
            self._cleanup(telegram)
            return

        if not self.spot:
            # show all
            reply_args = Markup.build(self.user, self.spot, self.origin)

            # edit message
            telegram.UpdateMessage(self.message, reply_args)

        elif not self.confirmed:
            # show confirmation
            reply_args = Markup.build(self.user, self.spot, self.origin)

            # edit message
            telegram.UpdateMessage(self.message, reply_args)
        else:
            # remove alert from user setting
            result = UserActions.RemoveAlert(self.user, self.spot)

            # show addition resul
            self._show_settings(telegram, result)


class Markup:
    @staticmethod
    def build(user: UserObjectProxy, spotid: str, origin: int) -> Dict[str, Any]:

        markup = InlineKeyboardMarkup()
        text = Strings.ActionRemoveAlert.SPOTS_TO_REMOVE_CAPTION

        if spotid:
            markup = BaseAction.Markup.build_yes_no_keyboard(Strings.ActionRemoveAlert.NAME, {
                    Strings.ActionRemoveAlert.DATA_SPOT_ID_NAME: spotid,
                    Strings.ActionRemoveAlert.DATA_CONFIRM: True,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                }, {
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                })
            return BaseAction.Markup.build(
                Strings.ActionRemoveAlert.SPOT_REMOVE_CONFIRM.format(spot=spotid),
                markup)

        if not user:
            text = UserActions.Strings.USER_NOT_FOUND_MESSAGE
        else:
            alerts = user.alerts
            if len(alerts) > 0:
                for alert in alerts:
                    alert_button = BaseAction.Markup.create_button(
                        Strings.ActionRemoveAlert.DELETE_BUTTON_TEXT.format(name=alert.spot_name, id=alert.spot_id),
                        Strings.ActionRemoveAlert.NAME, {
                            Strings.ActionRemoveAlert.DATA_SPOT_ID_NAME: alert.spot_id,
                            Strings.ActionBase.DATA_ORIGIN_NAME: origin
                        })

                    markup.row(alert_button)
            else:
                text = Strings.ActionRemoveAlert.NO_SPOTS_TO_REMOVE

            cancel_button = BaseAction.Markup.close_button(
                Strings.ActionRemoveAlert.NAME, {
                    Strings.ActionRemoveAlert.DATA_CANCEL: True,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                })

            markup.row(cancel_button)

        return BaseAction.Markup.build(text, markup)
