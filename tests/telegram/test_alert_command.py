from tests.telegram.conftest import assert_sent_message
from unittest.mock import patch

from telegram.message_utils import USER_SETTINGS_ALERTS_MESSAGE_FORMAT
from telegram.callback_actions import Strings
from actions.user_actions import UserActions


def test_windbot_alert_command_info(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/alert"))
    assert_sent_message('Usage: `/alert')

    wind_bot.OnWebHook(create_webhook_message("/alert 1"))
    assert_sent_message(Strings.ActionAddAlert.SET_SPOT_WINDSPEED_TEXT.format(
        id='1',
        speed=''
    ))

    wind_bot.OnWebHook(create_webhook_message("/alert None"))
    assert_sent_message(UserActions.Strings.SPOT_NOT_FOUND_MESSAGE)

    wind_bot.OnWebHook(create_webhook_message("/alert 1 2"))
    assert_sent_message('Usage: `/alert')


@patch('data.user.UserObjectProxy.ALERTS_LIMIT', 0)
def test_windbot_alert_command_invalid_args(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/alert spotId knots any"))
    assert_sent_message('Wind speed must be number')

    wind_bot.OnWebHook(create_webhook_message("/alert spotId 10 aaaas"))
    assert_sent_message('Invalid format for wind')

    wind_bot.OnWebHook(create_webhook_message("/alert spotId 10 aa-bbb"))
    assert_sent_message("'aa' is not recognizable")

    wind_bot.OnWebHook(create_webhook_message("/alert spotId 10 10-bbb"))
    assert_sent_message("'bbb' is not recognizable")

    wind_bot.OnWebHook(create_webhook_message("/alert spotId 10 SW-XX"))
    assert_sent_message("'XX' is not recognizable")

    wind_bot.OnWebHook(create_webhook_message("/alert spotId 10 SW-SS"))
    assert_sent_message("'SS' is not recognizable")

    wind_bot.OnWebHook(create_webhook_message("/alert spotId 10 777-SS"))
    assert_sent_message("'777' is not valid wind direction")

    wind_bot.OnWebHook(create_webhook_message("/alert spotId 10 SW-999"))
    assert_sent_message("'999' is not valid wind direction")

    wind_bot.OnWebHook(create_webhook_message("/alert none 10 WSW-NE"))
    assert_sent_message('The spot with such ID is unknown')

    wind_bot.OnWebHook(create_webhook_message("/alert spot 10 WSW-NE"))
    assert_sent_message('you are not allowed to have one more')


def test_windbot_alert_command_add_update_remove(wind_bot, telegram_env, create_webhook_message):

    def __assert_spot_set(name, spot, speed, wind_from, wind_to):
        search_string = USER_SETTINGS_ALERTS_MESSAGE_FORMAT.format(
            name=name,
            id=spot,
            ws=speed,
            wd_s=wind_from,
            wd_f=wind_to)
        assert_sent_message(search_string)

    # add
    wind_bot.OnWebHook(create_webhook_message("/alert spot1 10 N-S"))
    assert_sent_message('Alert was set')

    wind_bot.OnWebHook(create_webhook_message("/settings"))
    __assert_spot_set('Name_spot1', 'spot1', '10', '0', '180')

    # add
    wind_bot.OnWebHook(create_webhook_message("/alert spot2 10 359-S"))
    assert_sent_message('Alert was set')

    wind_bot.OnWebHook(create_webhook_message("/settings"))
    __assert_spot_set('Name_spot2', 'spot2', '10', '359', '180')

    # update
    wind_bot.OnWebHook(create_webhook_message("/alert spot2 20 W-E"))
    assert_sent_message('Alert was set')

    wind_bot.OnWebHook(create_webhook_message("/settings"))
    __assert_spot_set('Name_spot2', 'spot2', '20', '270', '90')

    # remove
    wind_bot.OnWebHook(create_webhook_message("/remove spot2"))
    assert_sent_message('Alert was removed')

    wind_bot.OnWebHook(create_webhook_message("/settings"))
    assert_sent_message('spot2', False)
    assert_sent_message('spot1')

    # remove
    wind_bot.OnWebHook(create_webhook_message("/remove spot1"))
    assert_sent_message('Alert was removed')

    wind_bot.OnWebHook(create_webhook_message("/settings"))
    assert_sent_message('spot2', False)
    assert_sent_message('spot1', False)


def test_windbot_command_remove_user_not_found(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/alert xxx 1 0-180", 4321))
    assert_sent_message("You wasn't found in database")
