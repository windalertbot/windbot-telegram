import aws_lambda_factory

from typing import List

from interfaces import IWindProvider
from data.spot import SpotInfo, SpotSearchResult, SpotWeather


class SpotActions:

    @staticmethod
    def GetSpotInfo(spot_id: str) -> SpotInfo:
        return SpotActions.windprovider().getSpotInfo(spot_id)

    @staticmethod
    def SearchSpotByName(search_str: str) -> SpotSearchResult:
        return SpotActions.windprovider().searchSpotByName(search_str)

    @staticmethod
    def GetSpotWeather(spot_ids: List[str]) -> List[SpotWeather]:
        return SpotActions.windprovider().getCurrentState(spot_ids)

    @staticmethod
    def windprovider() -> IWindProvider:
        return aws_lambda_factory.Factory.AccessWindprovider()
