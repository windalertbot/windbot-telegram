from errors import CommonErrors

from telegram.callback_actions import Strings
from telegram.callback_actions.close import CloseAction


class WeatherAction(CloseAction):

    def getErrorText(self) -> str:
        return CommonErrors.NO_WEATHER_DATA

    def getActionName(self):
        return Strings.ActionWeather.NAME
