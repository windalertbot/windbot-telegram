class JsonObjectProxy(object):
    def __init__(self, json):
        super().__init__()
        self._json = json

    def __getitem__(self, key):
        return self._json.get(key, None)

    def __setitem__(self, key, value):
        self._json[key] = value
