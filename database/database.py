import boto3

import logging

from typing import List
from datetime import datetime
from data.user import UserObjectProxy
from database.dynamo_db_init import create_users_table
from botocore.exceptions import ClientError
from boto3.dynamodb.conditions import Attr

logger = logging.getLogger(__name__)


class Database(object):
    USERS_TABLE_NAME = 'Users'

    def __init__(self, endpoint_url=None):
        super().__init__()

        self.__dynamodb_resource = boto3.resource('dynamodb', endpoint_url=endpoint_url)
        self.__dynamodb_client = boto3.client('dynamodb', endpoint_url=endpoint_url)

        self.__usersTableExist = None

    def getUsersToNotify(self) -> List[UserObjectProxy]:
        """
            Returns only users with spots and not recevied notifiaction this day
        """
        users_table = self._UsersTable

        all_users = []
        done = False
        start_key = None

        unix_utc_now = int(datetime.utcnow().timestamp())
        filter_expression = Attr('alerts').size().gt(0) & Attr('snoozetime').lt(unix_utc_now)

        while not done:

            if start_key:
                response = users_table.scan(
                    ExclusiveStartKey=start_key,
                    FilterExpression=filter_expression)
            else:
                response = users_table.scan(
                    FilterExpression=filter_expression)

            items = response.get('Items', [])
            all_users.extend([UserObjectProxy(i) for i in items])

            start_key = response.get('LastEvaluatedKey', None)
            done = start_key is None

        return all_users

    def getUsersBatch(self, chat_ids: List[int]) -> List[UserObjectProxy]:
        """
            Return collection of users by provided ids.
            Much more efficient method rather than get them one by one
        """
        result = []

        batch_size = 90
        for subset in [chat_ids[i:i + batch_size] for i in range(0, len(chat_ids), batch_size)]:
            try:
                response = self.__dynamodb_resource.batch_get_item(
                    RequestItems={
                        Database.USERS_TABLE_NAME: {
                            'Keys': [
                                {'chat_id': id} for id in subset
                            ],
                            'ConsistentRead': True
                        }
                    }
                )
            except ClientError as e:
                logger.error(f"Cannot get users({len(chat_ids)}) via batch get:\n{e}")
            else:
                for item in response['Responses'][Database.USERS_TABLE_NAME]:
                    result.append(UserObjectProxy(item))

        return result

    def getUser(self, chat_id: int) -> UserObjectProxy:
        try:
            response = self._UsersTable.get_item(Key={'chat_id': chat_id})
            item = response.get('Item', None)
            if item:
                return UserObjectProxy(item)

            logger.warning(f"User ({chat_id}) not found")

        except ClientError as e:
            logger.error(f"Error when trying to find user ({chat_id}).\n{e.response['Error']['Message']}")

        return None

    def createUser(self, chat_id: int, timezone: str = None, **kwargs) -> None:
        """
            Create user in database.
            In kwargs 'alerts' may be set as list of dict objects
        """
        alerts = kwargs.get('alerts', [])
        assert isinstance(alerts, list)

        user = {
            'chat_id': chat_id,
            'snoozetime': 0,
            'timezone': timezone,
            'notify': [10, 18],
            'alerts': alerts
        }

        self._UsersTable.put_item(Item=user)

    def updateUser(self, user: UserObjectProxy) -> bool:
        try:
            self._UsersTable.put_item(Item=user._json)
            return True
        except ClientError as e:
            logger.error(f"Update user ({user.chat_id}) failed.\n{e.response['Error']['Message']}")
            return False

    def removeUser(self, chat_id: int) -> bool:
        try:
            self._UsersTable.delete_item(
                Key={
                    'chat_id': chat_id
                }
            )
            return True
        except ClientError as e:
            logger.error(f"Removing user ({chat_id}) failed.\n{e.response['Error']['Message']}")
            return False

    @property
    def _UsersTable(self):
        if self.__usersTableExist is None:
            self.__usersTableExist = self._createOrDescribeUsersTable()
        return self.__dynamodb_resource.Table(Database.USERS_TABLE_NAME)

    def _createOrDescribeUsersTable(self):
        try:
            return self.__dynamodb_client.describe_table(TableName=Database.USERS_TABLE_NAME)
        except self.__dynamodb_client.exceptions.ResourceNotFoundException:
            logger.warning("Users table not found. Creating new one")
            table = create_users_table(self.__dynamodb_resource)
            table.meta.client.get_waiter('table_exists').wait(TableName=Database.USERS_TABLE_NAME)
            return table
