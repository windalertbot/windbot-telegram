from errors import CommonErrors


class Strings:

    class Keyboard:
        NUMBERS_EMOJI = ['0️⃣', '1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣']

        BUTTON_YES = '✅ YES'
        BUTTON_NO = '❌ NO'

        BUTTON_BACK = '◀️ Back '
        BUTTON_CANCEL = '❌ Cancel ❌'
        BUTTON_CLOSE = '❎ Close ❎'

    class ActionsFactory:
        ACTION_FIELD_NAME = 'a'

    class ActionBase:
        DATA_ORIGIN_NAME = 'o'

    class ActionSettings:
        NAME = 'ste'
        DATA_DO_KEY = 'do'
        DATA_EDIT_NAME = 'ed'
        DATA_SHOW_NAME = 'sw'

        USER_SETTINGS_CALLBACK_SELECT_EDIT_OPTION = """`        `Select option for edit:`         `"""

    class ActionTimezone:
        NAME = 'TZ'  # 64-bytes limit. Everything mustbe small.

        DATA_MODE_NAME = 'm'
        DATA_CURRENT_VALUE_NAME = 'c'
        DATA_CONFIRMATION_NAME = 's'

        SET_TZ_MODE_GMT = 'G'
        SET_TZ_MODE_HOUR = 'H'

        USER_SETTINGS_CALLBACK_SET_TZ_INIT = """`    `Choose method for timezone setup.`    `"""
        USER_SETTINGS_CALLBACK_SET_GMT_TZ_INIT = """Is your local time is advance GMT?"""
        USER_SETTINGS_CALLBACK_SET_GMT_TZ_NEXT = """`    `What is your time zone?`    `"""

        USER_SETTINGS_CALLBACK_SET_HOUR_TZ_INIT = """`    `What hour is it now?`    `
```
ex.
If it's 7 o'clock in the morning please enter 07.
If it's 8 o'clock in the evening please enter 20.
```
"""
        USER_SETTINGS_CALLBACK_SET_HOUR_TZ_NEXT = """`        `What hour is it now?`        `
> {now}\\_"""
        USER_SETTINGS_CALLBACK_SET_TZ_CONFIRM = """Your local time now is: {time} . Is that correct?"""

    class ActionRemoveAlert:
        NAME = 'rma'  # 64-bytes limit. Everything mustbe small.
        DATA_SPOT_ID_NAME = 'sid'
        DATA_CANCEL = 'cancel'
        DATA_CONFIRM = 'cf'

        NO_SPOTS_TO_REMOVE = CommonErrors.USER_HAS_NO_ALERTS
        SPOTS_TO_REMOVE_CAPTION = """`    `Select spot to remove.`    `"""
        SPOT_REMOVE_CONFIRM = """`    `Remove spot: *{spot}* ?`    `"""

        DELETE_BUTTON_TEXT = '❌ {name} ({id}) ❌'

    class ActionNotifytime:
        NAME = 'snt'  # 64-bytes limit. Everything mustbe small.

        DATA_START_TIME_NAME = 'st'
        DATA_END_TIME_NAME = 'en'
        DATA_CONFIMED_NAME = 'cn'

        USER_SETTINGS_NOTIFY_SET_INIT = """Please enter select notification hours in 24h format.
```
ex.
If you want notification starts at 7 o'clock in the morning please enter 07.
If you want notification ends at 8 o'clock in the evening please enter 20.
```
#️⃣#️⃣ : 0️⃣0️⃣  ➖  #️⃣#️⃣ : 0️⃣0️⃣
"""

        USER_SETTINGS_NOTIFY_SET_START_HOUR = """`    `Please, enter notifications start time:`    `

{start} : 0️⃣0️⃣  ➖  #️⃣#️⃣ : 0️⃣0️⃣"""

        USER_SETTINGS_NOTIFY_SET_END_HOUR = """`    `Please, enter notifications end time:`    `

{start} : 0️⃣0️⃣  ➖  {end} : 0️⃣0️⃣"""

        USER_SETTINGS_NOTIFY_SET_CONFIRM = """`    `Notifications will be sent:`    `

{start} : 0️⃣0️⃣  ➖  {end} : 0️⃣0️⃣

Is it ok?
"""

    class ActionFindAlert:
        NAME = 'fnd'  # 64-bytes limit. Everything mustbe small.

        DATA_CANCEL = 'cancel'
        DATA_SEARCH_STR = 'ss'
        DATA_COUNTRY_CODE = 'c'

        SELECT_COUNTRY_REPLY_TEXT = """ Searching: `'{search}'`

Please select country where your spot located:
"""

        SELECT_SPOT_REPLY_TEXT = """ Searching: `{search}`
Selected country: `{country}`

Please select spot:
"""

    class ActionAddAlert:
        NAME = 'AA'  # 64-bytes limit. Everything mustbe small.

        # keys
        DATA_CANCEL = 'cancel'
        DATA_SPOT_ID = 'i'
        DATA_SPOT_NAME = 'n'
        DATA_SPOT_COUNTRY = 'c'

        DATA_CONFIRMED = 'f'
        DATA_WIND_SPEED = 's'
        DATA_WIND_DIR = 'd'

        # values
        CONFIRMED_SPEED = '0'
        CONFIRMED_DIRECTION = '1'
        CONFIRMED_ALERT = '2'

        # texts
        SET_SPOT_WINDSPEED_TEXT = """Adding spot: *{id}*
Please, enter desired windspeed in knots.
 > {speed}
"""
        SET_SPOT_WINDSPEED_CONFIRM = """Adding spot: *{id}*
Set alert wind speed to: *{speed}*kts ?
"""

        SET_SPOT_WINDDIR_TEXT_START = """Adding spot: *{id}*
Alert speed: *{speed}*kts

Please, select desired wind direction starts from:
"""

        SET_SPOT_WINDDIR_TEXT_END = """Adding spot: *{id}*
Alert speed: *{speed}*kts
Alert direction:
{wdir}
Please, select desired wind direction ends with:
"""

        SET_SPOT_WINDDIR_TEXT_CONFIRM = """Adding spot: *{id}*
Alert speed: *{speed}*kts

Set wind direction alert at *{wdir}*?
{wdir_map}
"""

        SET_SPOT_ALERT_TEXT_CONFIRM = """Adding spot: *{id}*
Alert speed: *{speed}*kts
Alert direction: *{wdir}*
{wdir_map}
Is that correct?
"""

    class ActionForget:
        NAME = 'frg'

        DATA_ACTION_CONFIRMED = 'cnf'

        ACTION_REMOVE = 'remove'
        ACTION_CANCEL = 'cancel'

        FORGET_ACTION_TEXT = "Are you sure you want delete yourself from database?"

    class ActionWeather:
        NAME = 'wth'

        TEXT_CLEAR = 'Clear ☀️'
        TEXT_PARTLY_CLODY = 'Partly cloudy 🌤'
        TEXT_CLOUDY = 'Cloudy ⛅️'
        TEXT_OVERCAST = 'Overcast ☁️'

    class ActionClose:
        NAME = 'cls'
        DATA_ACTION_CLOSE = 'close'
