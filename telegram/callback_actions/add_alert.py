import logging
import utils

from typing import Dict, Any

from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction

from actions.user_actions import UserActions

logger = logging.getLogger(__name__)


class AddAlertAction(BaseAction):

    def __init__(self, message, data):
        super().__init__(message, data)
        self.spot = data.get(Strings.ActionAddAlert.DATA_SPOT_ID, None)
        self.speed = data.get(Strings.ActionAddAlert.DATA_WIND_SPEED, None)
        self.wdir = data.get(Strings.ActionAddAlert.DATA_WIND_DIR, None)

        self.cancel = data.get(Strings.ActionAddAlert.DATA_CANCEL, None)
        self.setvars = data.get(Strings.ActionAddAlert.DATA_CONFIRMED, None)

    def process(self, telegram, **kwargs) -> None:
        if not self.spot or self.cancel:
            self._cleanup(telegram)
            if not self.cancel:
                logger.error(f'Add alert action was triggered without spot id')
            return

        if self.setvars == Strings.ActionAddAlert.CONFIRMED_ALERT:
            text = UserActions.AddAlert(self.user, self.spot, self.speed, self.wdir)
            self._show_settings(telegram, text)
        else:
            reply_args = Markup.build(self.setvars, self.spot, self.speed, self.wdir, self.origin)
            telegram.UpdateMessage(self.message, reply_args)


class Markup:

    @staticmethod
    def build(setvars: str, spot_id: str, speed: str, wdir: str, origin: int) -> Dict[str, Any]:
        if not setvars:
            # build speed markup
            if not speed or len(speed) < 2:
                return Markup.__build_wind_speed(spot_id, speed, origin)
            else:
                return Markup.__build_wind_speed_confirm(spot_id, speed, origin)
        elif setvars == Strings.ActionAddAlert.CONFIRMED_SPEED:
            # build direction markup
            if not wdir or '-' not in wdir:
                return Markup.__build_wind_dir(spot_id, speed, wdir, origin)
            else:
                return Markup.__build_wind_dir_confirm(spot_id, speed, wdir, origin)

        # build alert confirmation
        return Markup.__build_alert_confirm(spot_id, speed, wdir, origin)

    @staticmethod
    def __build_wind_speed(spot_id: str, speed: str, origin: int) -> Dict[str, Any]:
        markup = BaseAction.Markup.build_numeric_keyboard(
            Strings.ActionAddAlert.NAME,
            lambda num: {
                Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                Strings.ActionAddAlert.DATA_WIND_SPEED: speed + str(num) if speed else str(num),
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            },
            button_prefix=BaseAction.Markup.num_to_emoji(speed) if speed else None)

        if not speed:
            markup.row(BaseAction.Markup.cancel_button(Strings.ActionAddAlert.NAME, {
                    Strings.ActionAddAlert.DATA_CANCEL: True,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }))
        else:
            markup.row(BaseAction.Markup.back_button(Strings.ActionAddAlert.NAME, {
                    Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }))

        text = Strings.ActionAddAlert.SET_SPOT_WINDSPEED_TEXT.format(
            id=spot_id,
            speed=speed if speed else '')

        return BaseAction.Markup.build(text, markup)

    @staticmethod
    def __build_wind_speed_confirm(spot_id: str, speed: str, origin: int) -> Dict[str, Any]:
        markup = BaseAction.Markup.build_yes_no_keyboard(Strings.ActionAddAlert.NAME, {
                Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                Strings.ActionAddAlert.DATA_WIND_SPEED: speed,
                Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }, {
                Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })
        text = Strings.ActionAddAlert.SET_SPOT_WINDSPEED_CONFIRM.format(
            id=spot_id,
            speed=speed)

        return BaseAction.Markup.build(text, markup)

    @staticmethod
    def __build_wind_dir(spot_id: str, speed: str, wdir: str, origin: int) -> Dict[str, Any]:
        wind_keyboard = [
                'NW', 'N', 'NE',
                'W', '🧭', 'E',
                'SW', 'S', 'SE',
            ]
        if not wdir:
            markup = BaseAction.Markup.build_keyboard(
                wind_keyboard,
                Strings.ActionAddAlert.NAME,
                lambda button: {
                    Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                    Strings.ActionAddAlert.DATA_WIND_SPEED: speed,
                    Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
                    Strings.ActionAddAlert.DATA_WIND_DIR: button if button != '🧭' else None,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                },
                row_width=3)

            markup.row(BaseAction.Markup.back_button(Strings.ActionAddAlert.NAME, {
                    Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                    Strings.ActionAddAlert.DATA_WIND_SPEED: speed,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }))

            return BaseAction.Markup.build(
                Strings.ActionAddAlert.SET_SPOT_WINDDIR_TEXT_START.format(
                    id=spot_id,
                    speed=speed),
                markup)
        else:
            markup = BaseAction.Markup.build_keyboard(
                wind_keyboard,
                Strings.ActionAddAlert.NAME,
                lambda button: {
                    Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                    Strings.ActionAddAlert.DATA_WIND_SPEED: speed,
                    Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
                    Strings.ActionAddAlert.DATA_WIND_DIR: f"{wdir}-{button}" if button != '🧭' else wdir,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                },
                row_width=3)

            markup.row(BaseAction.Markup.back_button(Strings.ActionAddAlert.NAME, {
                    Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                    Strings.ActionAddAlert.DATA_WIND_SPEED: speed,
                    Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin}))

            return BaseAction.Markup.build(
                Strings.ActionAddAlert.SET_SPOT_WINDDIR_TEXT_END.format(
                    id=spot_id,
                    speed=speed,
                    wdir=utils.getWindDirEmoji(wdir)),
                markup)

    @staticmethod
    def __build_wind_dir_confirm(spot_id: str, speed: str, wdir: str, origin: int) -> Dict[str, Any]:
        markup = BaseAction.Markup.build_yes_no_keyboard(Strings.ActionAddAlert.NAME, {
            Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
            Strings.ActionAddAlert.DATA_WIND_SPEED: speed,
            Strings.ActionAddAlert.DATA_WIND_DIR: wdir,
            Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_DIRECTION,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
        }, {
            Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
            Strings.ActionAddAlert.DATA_WIND_SPEED: speed,
            Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_SPEED,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
        })

        return BaseAction.Markup.build(
            Strings.ActionAddAlert.SET_SPOT_WINDDIR_TEXT_CONFIRM.format(
                id=spot_id,
                speed=speed,
                wdir=wdir,
                wdir_map=utils.getWindDirEmoji(wdir)),
            markup)

    @staticmethod
    def __build_alert_confirm(spot_id: str, speed: str, wdir: str, origin: int) -> Dict[str, Any]:
        markup = BaseAction.Markup.build_yes_no_keyboard(Strings.ActionAddAlert.NAME, {
                Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                Strings.ActionAddAlert.DATA_WIND_SPEED: speed,
                Strings.ActionAddAlert.DATA_WIND_DIR: wdir,
                Strings.ActionAddAlert.DATA_CONFIRMED: Strings.ActionAddAlert.CONFIRMED_ALERT,
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }, {
                Strings.ActionAddAlert.DATA_SPOT_ID: spot_id,
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })
        return BaseAction.Markup.build(
            Strings.ActionAddAlert.SET_SPOT_ALERT_TEXT_CONFIRM.format(
                id=spot_id,
                speed=speed,
                wdir=wdir,
                wdir_map=utils.getWindDirEmoji(wdir)),
            markup)
