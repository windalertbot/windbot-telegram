import pytest

from database.database import Database
from data.alert import AlertObjectProxy
from datetime import datetime


@pytest.fixture
def clean_users_table(database_fixture):
    database_fixture._Database__usersTableExist = None  # reset state
    database_fixture._UsersTable.delete()  # delete before
    database_fixture._Database__usersTableExist = None  # reset state
    yield
    database_fixture._Database__usersTableExist = None  # reset state
    database_fixture._UsersTable.delete()  # delete after
    database_fixture._Database__usersTableExist = None  # reset state


@pytest.fixture
def dynamodb(database_fixture):
    assert isinstance(database_fixture, Database)
    return database_fixture


def test_dynamodb(dynamodb, clean_users_table):
    dynamodb.createUser(1)

    u1 = dynamodb.getUser(1)
    assert u1.chat_id == 1
    assert u1.timezone is None
    assert len(u1.alerts) == 0

    u1.addAlert(AlertObjectProxy({
        'id': "cy86",
        'desired_wind_speed': 15,
        'desired_wind_direction': [180, 330],
        'name': 'name_from_api'
        }))

    u1.addAlert(AlertObjectProxy({
        'id': "us2502",
        'desired_wind_speed': 10,
        'desired_wind_direction': [300, 15],
        }))

    u1['timezone'] = "GMT+9"

    assert dynamodb.updateUser(u1)
    u1_upd = dynamodb.getUser(u1.chat_id)
    assert len(u1_upd.alerts) == 2
    assert u1_upd.alerts[1].spot_id == "us2502"

    # remove spot
    u1_upd['alerts'].remove(next((s for s in u1_upd['alerts'] if s['id'] == 'us2502'), None))

    assert dynamodb.updateUser(u1_upd)
    u1_upd = dynamodb.getUser(u1.chat_id)

    assert u1_upd.chat_id == u1.chat_id
    assert len(u1_upd.alerts) == 1
    assert u1_upd.alerts[0].spot_id == "cy86"
    assert u1_upd.alerts[0].desired_wind_direction == [180, 330]
    assert u1_upd.alerts[0].desired_wind_speed == 15

    u = dynamodb.getUsersToNotify()

    assert(len(u) == 1)
    assert(u[0].chat_id is not None)
    assert(u[0].timezone is not None)
    assert(u[0].alerts is not None)

    assert(len(u[0].alerts) == 1)
    assert(u[0].alerts[0].spot_id is not None)

    assert dynamodb.removeUser(u1.chat_id)
    u1_upd = dynamodb.getUser(u1.chat_id)
    assert u1_upd is None


def test_create_user_with_params(dynamodb, clean_users_table):
    dynamodb.createUser(1, alerts=[])

    u = dynamodb.getUsersToNotify()
    assert len(u) == 0, "Invalid user creation"

    dynamodb.createUser(2, alerts=[{
        'id': "xxx",
        'desired_wind_speed': 1,
        'desired_wind_direction': [180, 330],
        }])

    u = dynamodb.getUsersToNotify()
    assert len(u) == 1, "Invalid user creation"


def test_get_users(dynamodb, clean_users_table):
    dynamodb.createUser(1)
    dynamodb.createUser(2)

    u = dynamodb.getUsersToNotify()
    assert len(u) == 0, "Invalid user creation"

    u1 = dynamodb.getUser(1)
    u1.addAlert(AlertObjectProxy({
        'id': "xxx",
        'desired_wind_speed': 1,
        'desired_wind_direction': [180, 330],
        }))
    dynamodb.updateUser(u1)

    u = dynamodb.getUsersToNotify()
    assert len(u) == 1

    u2 = dynamodb.getUser(2)
    u2.addAlert(AlertObjectProxy({
        'id': "zzz",
        'desired_wind_speed': 1,
        'desired_wind_direction': [180, 330],
        }))
    dynamodb.updateUser(u2)

    u = dynamodb.getUsersToNotify()
    assert len(u) == 2

    exclude_id = u[0].chat_id
    u[0]['snoozetime'] = int(datetime.utcnow().timestamp()) + 9999
    dynamodb.updateUser(u[0])

    u2 = dynamodb.getUsersToNotify()
    assert len(u2) == 1
    assert u2[0].chat_id != exclude_id


def test_get_batch_users(dynamodb, clean_users_table):
    for i in range(0, 200):
        dynamodb.createUser(i)

    requested_id = list(range(20, 150))

    result = dynamodb.getUsersBatch(requested_id)
    assert len(result) == len(requested_id)

    for user in result:
        assert user.chat_id in requested_id, f"User with chat id:{user.chat_id} wasnt returned by database"

    requested_id = [1, 128, 798]
    result = dynamodb.getUsersBatch(requested_id)
    assert len(result) == 2
    for user in result:
        assert user.chat_id != 798


def test_user_snooze(dynamodb, clean_users_table):
    dynamodb.createUser(1)
    dynamodb.createUser(2, "GMT+2")
    dynamodb.createUser(3, "GMT-2")

    user1 = dynamodb.getUser(1)
    user2 = dynamodb.getUser(2)
    user3 = dynamodb.getUser(3)
    assert user1.snoozetime == 0
    assert user2.snoozetime == 0
    assert user3.snoozetime == 0

    user1.snooze()
    user2.snooze()
    user3.snooze()

    dynamodb.updateUser(user1)
    dynamodb.updateUser(user2)
    dynamodb.updateUser(user3)

    user1 = dynamodb.getUser(1)
    user2 = dynamodb.getUser(2)
    user3 = dynamodb.getUser(3)

    assert user1.snoozetime != 0
    assert user2.snoozetime != 0
    assert user3.snoozetime != 0

    assert user1.snoozetime != user3.snoozetime
    assert user2.snoozetime != user1.snoozetime
