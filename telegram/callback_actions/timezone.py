import logging

from typing import Dict, Any
from datetime import datetime
from dateutil import tz
from telebot.types import InlineKeyboardMarkup

from actions.user_actions import UserActions

from interfaces import IWindbot
from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction

logger = logging.getLogger(__name__)


class SetTimezoneAction(BaseAction):
    def __init__(self, message, data):
        super().__init__(message, data)

        self.__mode = data.get(Strings.ActionTimezone.DATA_MODE_NAME, None)
        self.__current = data.get(Strings.ActionTimezone.DATA_CURRENT_VALUE_NAME, None)
        self.__confirmed = data.get(Strings.ActionTimezone.DATA_CONFIRMATION_NAME, None)

    @property
    def mode(self) -> str:
        return self.__mode

    @property
    def current_value(self) -> str:
        return self.__current

    @property
    def isConfirmed(self) -> bool:
        return self.__confirmed

    def process(self, telegram: IWindbot, **kwargs) -> Dict[str, Any]:
        if self.isConfirmed:

            text = None
            if self.mode in [Strings.ActionTimezone.SET_TZ_MODE_GMT, Strings.ActionTimezone.SET_TZ_MODE_HOUR]:
                text = UserActions.SetUserTimezone(self.user, self.current_value)
            else:
                logger.error(f"Callback action `/setting timezone` error:\
                    confirmation received but mode:'{self.mode}' is invalid")
            self._show_settings(telegram, text)

        else:
            # re-create reply markup for message
            reply_args = Markup.build(self.mode, self.current_value, self.origin)

            # edit message
            telegram.UpdateMessage(self.message, reply_args)


class Markup:
    @staticmethod
    def build(mode: str, current: str, origin: int) -> Dict[str, Any]:
        if not mode:
            return Markup.__build_callback_data_set_timezone_mode_init(origin)
        elif mode == Strings.ActionTimezone.SET_TZ_MODE_GMT:
            if not current:
                return Markup.__build_callback_data_set_timezone_gmt_init(mode, origin)
            elif current.endswith('-') or current.endswith('+'):
                return Markup.__build_callback_data_set_timezone_gmt_next(current, mode, origin)
            else:
                return Markup.__build_callback_data_set_timezone_gmt_confirm(current, mode, origin)
        else:
            if not current:
                return Markup.__build_callback_data_set_timezone_hour_init(mode, origin)
            elif len(current) == 1:
                return Markup.__build_callback_data_set_timezone_hour_next(current, mode, origin)
            else:
                return Markup.__build_callback_data_set_timezone_hour_confirm(current, mode, origin)

    @staticmethod
    def __build_callback_data_set_timezone_mode_init(origin: int) -> Dict[str, Any]:
        markup = InlineKeyboardMarkup()

        button_set_gmt = BaseAction.Markup.create_button('Enter GMT offset', Strings.ActionTimezone.NAME, {
            Strings.ActionTimezone.DATA_MODE_NAME: Strings.ActionTimezone.SET_TZ_MODE_GMT,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin,
            })

        button_set_hour = BaseAction.Markup.create_button('Enter local hour', Strings.ActionTimezone.NAME, {
            Strings.ActionTimezone.DATA_MODE_NAME: Strings.ActionTimezone.SET_TZ_MODE_HOUR,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin,
            })

        button_back = BaseAction.Markup.back_button(Strings.ActionSettings.NAME, {
            Strings.ActionSettings.DATA_DO_KEY: Strings.ActionSettings.DATA_EDIT_NAME,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin,
            })

        markup.row(button_set_hour, button_set_gmt).row(button_back)

        return BaseAction.Markup.build(
            Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_TZ_INIT,
            markup)

    @staticmethod
    def __build_callback_data_set_timezone_gmt_init(mode: str, origin: int) -> Dict[str, Any]:
        markup = InlineKeyboardMarkup()

        button_yes_plus = BaseAction.Markup.create_button('Yes (GMT+)', Strings.ActionTimezone.NAME, {
            Strings.ActionTimezone.DATA_MODE_NAME: mode,
            Strings.ActionTimezone.DATA_CURRENT_VALUE_NAME: 'GMT+',
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })

        button_no_minus = BaseAction.Markup.create_button('No (GMT-)', Strings.ActionTimezone.NAME, {
            Strings.ActionTimezone.DATA_MODE_NAME: mode,
            Strings.ActionTimezone.DATA_CURRENT_VALUE_NAME: 'GMT-',
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })

        button_back = BaseAction.Markup.back_button(Strings.ActionTimezone.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })

        markup.row(button_yes_plus, button_no_minus).row(button_back)

        return BaseAction.Markup.build(
            Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_GMT_TZ_INIT,
            markup)

    @staticmethod
    def __build_callback_data_set_timezone_gmt_next(current: str, mode: str, origin: int) -> Dict[str, Any]:
        markup = BaseAction.Markup.build_numeric_keyboard(
            Strings.ActionTimezone.NAME,
            lambda num: {
                Strings.ActionTimezone.DATA_MODE_NAME: mode,
                Strings.ActionTimezone.DATA_CURRENT_VALUE_NAME: f"{current}{num}",
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            },
            row_width=4,
            max_number=12,
            create_button_text=lambda num: f"{current}{num}"
        )

        markup.row(BaseAction.Markup.back_button(Strings.ActionTimezone.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }))

        return BaseAction.Markup.build(
            Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_GMT_TZ_NEXT.format(gmt=current),
            markup)

    @staticmethod
    def __build_callback_data_set_timezone_gmt_confirm(current: str, mode: str, origin: int) -> Dict[str, Any]:
        user_now = datetime.now(tz.gettz(current))
        time = f"{user_now.hour:02d}:{user_now.minute:02d}"

        return BaseAction.Markup.build(
            Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_TZ_CONFIRM.format(time=time),
            Markup.__build_yes_no_keyboard(current, mode, origin))

    @staticmethod
    def __build_callback_data_set_timezone_hour_init(mode: str, origin: int) -> Dict[str, Any]:
        markup = BaseAction.Markup.build_numeric_keyboard(
            Strings.ActionTimezone.NAME,
            lambda num: {
                Strings.ActionTimezone.DATA_MODE_NAME: mode,
                Strings.ActionTimezone.DATA_CURRENT_VALUE_NAME: str(num),
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            },
            row_width=3,
            max_number=3
        )

        button_back = BaseAction.Markup.back_button(Strings.ActionTimezone.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })

        markup.row(button_back)

        return BaseAction.Markup.build(
            Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_HOUR_TZ_INIT,
            markup)

    @staticmethod
    def __build_callback_data_set_timezone_hour_next(current: str, mode: str, origin: int) -> Dict[str, Any]:
        markup = BaseAction.Markup.build_numeric_keyboard(
            Strings.ActionTimezone.NAME,
            lambda num: {
                Strings.ActionTimezone.DATA_MODE_NAME: mode,
                Strings.ActionTimezone.DATA_CURRENT_VALUE_NAME: f"{current}{num}",
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            },
            max_number=4 if current == '2' else 10
        )

        markup.row(BaseAction.Markup.back_button(Strings.ActionTimezone.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }))

        return BaseAction.Markup.build(
            Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_HOUR_TZ_NEXT.format(now=current),
            markup)

    @staticmethod
    def __build_callback_data_set_timezone_hour_confirm(current: str, mode: str, origin: int) -> Dict[str, Any]:
        time = f"{current}:{datetime.utcnow().minute:02d}"
        return BaseAction.Markup.build(
            Strings.ActionTimezone.USER_SETTINGS_CALLBACK_SET_TZ_CONFIRM.format(time=time),
            Markup.__build_yes_no_keyboard(current, mode, origin))

    @staticmethod
    def __build_yes_no_keyboard(current: str, mode: str, origin: int) -> InlineKeyboardMarkup:
        return BaseAction.Markup.build_yes_no_keyboard(Strings.ActionTimezone.NAME, {
                Strings.ActionTimezone.DATA_MODE_NAME: mode,
                Strings.ActionTimezone.DATA_CURRENT_VALUE_NAME: current,
                Strings.ActionTimezone.DATA_CONFIRMATION_NAME: True,
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }, {
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })
