from typing import Dict, Any

from telebot.types import InlineKeyboardMarkup
from interfaces import IWindbot

from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction


class CloseAction(BaseAction):
    """
        This "action" is just a way to attach close button to simple sent message
    """
    def __init__(self, message, data):
        super().__init__(message, data)
        self.close = data.get(Strings.ActionClose.DATA_ACTION_CLOSE, None)

    def getActionName(self):
        return Strings.ActionClose.NAME

    def getErrorText(self):
        return ''

    def process(self, telegram: IWindbot, **kwargs) -> Dict[str, Any]:
        if self.close:
            self._cleanup(telegram)
        else:
            # re-create reply markup for message
            reply_args = Markup.build(self.getErrorText(), self.origin, self.getActionName())

            # edit message
            telegram.UpdateMessage(self.message, reply_args)


class Markup:
    @staticmethod
    def build(text: str, origin: int, action_name: str = None) -> Dict[str, Any]:
        if not action_name:
            action_name = Strings.ActionClose.NAME

        keyboard = InlineKeyboardMarkup()
        keyboard.row(BaseAction.Markup.close_button(
            action_name, {
                Strings.ActionClose.DATA_ACTION_CLOSE: True,
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }
        ))
        return BaseAction.Markup.build(text, keyboard)
