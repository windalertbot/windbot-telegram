import logging

from typing import Any, Dict

from interfaces import IWindbot
from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction

from actions.user_actions import UserActions

logger = logging.getLogger(__name__)


class NotifytimeCallbackAction(BaseAction):
    def __init__(self, message, data):
        super().__init__(message, data)

        self.start_time = data.get(Strings.ActionNotifytime.DATA_START_TIME_NAME, None)
        self.end_time = data.get(Strings.ActionNotifytime.DATA_END_TIME_NAME, None)
        self.is_confirmed = data.get(Strings.ActionNotifytime.DATA_CONFIMED_NAME, None)

    def process(self, telegram: IWindbot, **kwargs):
        if self.is_confirmed:
            chat_id = self.chatId

            text = UserActions.SetUserNotifytime(chat_id, f"[{self.start_time},{self.end_time}]")
            self._show_settings(telegram, text)

        else:
            # re-create reply markup for message
            reply_args = Markup.build(self.start_time, self.end_time, self.origin)

            # edit message
            telegram.UpdateMessage(self.message, reply_args)


class Markup:

    @staticmethod
    def build(start: str, end: str, origin: int) -> Dict[str, Any]:
        if not start or len(start) == 1:
            return Markup.__build_callback_data_start(start, origin)
        elif not end or len(end) == 1:
            return Markup.__build_callback_data_end(start, end, origin)
        else:
            return Markup.__build_callback_data_confirm(start, end, origin)

    @staticmethod
    def __build_callback_data_start(start: str, origin: int) -> Dict[str, Any]:
        if not start:
            text = Strings.ActionNotifytime.USER_SETTINGS_NOTIFY_SET_INIT
        else:
            text = Strings.ActionNotifytime.USER_SETTINGS_NOTIFY_SET_START_HOUR.format(
                start=BaseAction.Markup.num_to_emoji(start) + '#️⃣')

        markup = BaseAction.Markup.build_numeric_keyboard(
            Strings.ActionNotifytime.NAME,
            lambda num: {
                Strings.ActionNotifytime.DATA_START_TIME_NAME: str(num) if not start else f"{start}{num}",
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            },
            max_number=3 if not start else 4 if start == '2' else 10,
            button_prefix=None if not start else BaseAction.Markup.num_to_emoji(start)
        )

        markup.row(BaseAction.Markup.back_button(
            Strings.ActionNotifytime.NAME if start else Strings.ActionSettings.NAME,
            {Strings.ActionBase.DATA_ORIGIN_NAME: origin}))

        return BaseAction.Markup.build(text, markup)

    @staticmethod
    def __build_callback_data_end(start: str, end: str, origin: int) -> Dict[str, Any]:
        text = Strings.ActionNotifytime.USER_SETTINGS_NOTIFY_SET_END_HOUR.format(
            start=BaseAction.Markup.num_to_emoji(start),
            end='#️⃣#️⃣' if not end else BaseAction.Markup.num_to_emoji(end) + '#️⃣',
        )

        markup = BaseAction.Markup.build_numeric_keyboard(
            Strings.ActionNotifytime.NAME,
            lambda num: {
                Strings.ActionNotifytime.DATA_START_TIME_NAME: start,
                Strings.ActionNotifytime.DATA_END_TIME_NAME: str(num) if not end else f"{end}{num}",
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            },
            max_number=3 if not end else 4 if end == '2' else 10,
            button_prefix=None if not end else BaseAction.Markup.num_to_emoji(end)
        )

        markup.row(BaseAction.Markup.back_button(Strings.ActionNotifytime.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: origin}))

        return BaseAction.Markup.build(text, markup)

    @staticmethod
    def __build_callback_data_confirm(start: str, end: str, origin: int) -> Dict[str, Any]:
        markup = BaseAction.Markup.build_yes_no_keyboard(Strings.ActionNotifytime.NAME, {
                Strings.ActionNotifytime.DATA_START_TIME_NAME: start,
                Strings.ActionNotifytime.DATA_END_TIME_NAME: end,
                Strings.ActionNotifytime.DATA_CONFIMED_NAME: True,
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }, {
                Strings.ActionNotifytime.DATA_CONFIMED_NAME: False,
                Strings.ActionBase.DATA_ORIGIN_NAME: origin
            }
        )

        return BaseAction.Markup.build(
            Strings.ActionNotifytime.USER_SETTINGS_NOTIFY_SET_CONFIRM.format(
                start=BaseAction.Markup.num_to_emoji(start),
                end=BaseAction.Markup.num_to_emoji(end)),
            markup)
