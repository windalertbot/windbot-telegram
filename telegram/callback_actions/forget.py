from typing import Dict, Any

from actions.user_actions import UserActions
from interfaces import IWindbot

from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction


class ForgetAction(BaseAction):
    def __init__(self, message, data):
        super().__init__(message, data)
        self.confirmed = data.get(Strings.ActionForget.DATA_ACTION_CONFIRMED, None)

    def process(self, telegram: IWindbot, **kwargs) -> Dict[str, Any]:
        if not self.confirmed:
            # re-create reply markup for message
            reply_args = Markup.build(self.origin)

            # edit message
            telegram.UpdateMessage(self.message, reply_args)
        else:
            self._cleanup(telegram)

            if self.confirmed == Strings.ActionForget.ACTION_REMOVE:
                UserActions.RemoveUser(self.chatId)


class Markup:

    @staticmethod
    def build(origin: int) -> Dict[str, Any]:
        return BaseAction.Markup.build(
            Strings.ActionForget.FORGET_ACTION_TEXT,
            BaseAction.Markup.build_yes_no_keyboard(
                Strings.ActionForget.NAME,
                {
                    Strings.ActionForget.DATA_ACTION_CONFIRMED: Strings.ActionForget.ACTION_REMOVE,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                },
                {
                    Strings.ActionForget.DATA_ACTION_CONFIRMED: Strings.ActionForget.ACTION_CANCEL,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                }))
