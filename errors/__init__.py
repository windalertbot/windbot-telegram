class ParseError(BaseException):
    def __init__(self, message):
        super().__init__()
        self.message = message


class TelegramErrorCode:
    TOO_MANY_REQUESTS = 429


class CommonErrors:
    USER_HAS_NO_ALERTS = """You don't have any alerts yet. 😐"""

    NO_WEATHER_DATA = """Can't obtain weather data. 😦"""
