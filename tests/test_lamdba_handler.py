import os
import json
import pytest

from importlib import reload
from tests.telegram.conftest import assert_sent_message
from tests.conftest import LAST_SENT_MESSAGE_ENV, database_mock, telegram_mock, windprovider_mock

from aws_lambda_handler import lambda_handler
from aws_lambda_data import LambdaEventAction
from aws_lambda_factory import Factory

valid_context = None
valid_token = "VALID_TOKEN"


def create_event(_action, _payload, _token=valid_token):
    return json.loads('{{ "action":"{0}", "token":"{1}", "payload":"{2}" }}'.format(
        _action, _token, _payload))


# ----------------------------------------------------------------------------------------------------------------- #
@pytest.fixture
def patched_env(with_patched_telegram, with_patched_windprovider, monkeypatch):
    monkeypatch.setenv('TELEGRAM_TOKEN', valid_token)
    assert os.getenv('TELEGRAM_TOKEN') == valid_token
    import aws_lambda_handler
    reload(aws_lambda_handler)


# ----------------------------------------------------------------------------------------------------------------- #
def test_lambda_invalid_token(patched_env):
    result = lambda_handler(create_event(None, None, None), valid_context)
    assert result['status'] == 500
    if 'token' not in result['body']:
        assert False, "Expected token error. Got:" + result['body']


def test_lambda_invalid_action(patched_env):
    result = lambda_handler(create_event('invalid', None), valid_context)
    assert result['status'] == 500
    if 'No handlers for action:invalid' not in result['body']:
        assert False, "Expected missing handler error. Got:" + result['body']


def test_lambda_webhook_action(patched_env):
    result = lambda_handler(
        create_event(LambdaEventAction.WEBHOOK, '{ \\"update_id\\": 0 }'),
        valid_context)
    assert result['status'] == 200, result


def test_lambda_update_action(patched_env):
    result = lambda_handler(
        create_event(LambdaEventAction.UPDATE, None),
        valid_context)
    assert result['status'] == 200, result


def test_lambda_2_updates(patched_env, with_patched_windprovider,
                          with_patched_telegram, database_fixture, monkeypatch):
    database_fixture.createUser(1, alerts=[
        {
            'id': "xxx",
            'desired_wind_speed': 1,
            'desired_wind_direction': [180, 330],
        }
    ])

    result = lambda_handler(create_event(LambdaEventAction.UPDATE, None), valid_context)
    assert result['status'] == 200, result

    assert_sent_message('There is wind')

    # now imitating next trigger and there should no be any notification
    monkeypatch.delenv(LAST_SENT_MESSAGE_ENV)

    result = lambda_handler(create_event(LambdaEventAction.UPDATE, None), valid_context)
    assert result['status'] == 200, result

    assert os.getenv(LAST_SENT_MESSAGE_ENV) is None


def test_lambda_factory_valid_creation():
    db = Factory.AccessDatabase()
    assert db is not None
    assert not isinstance(db, database_mock)

    tg = Factory.AccessWindbot('any')
    assert tg is not None
    assert not isinstance(tg, telegram_mock)

    wp = Factory.AccessWindprovider()
    assert wp is not None
    assert not isinstance(wp, windprovider_mock)
