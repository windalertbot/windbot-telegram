from tests.telegram.conftest import assert_sent_message
from telegram.callback_actions import Strings


def test_callback_action_forget(wind_bot, telegram_env, test_user, database_fixture,
                                create_callback_action_message, assert_last_message_deleted):
    action = create_callback_action_message(Strings.ActionForget.NAME, {
        Strings.ActionBase.DATA_ORIGIN_NAME: 977,
        })

    wind_bot.OnWebHook(action)
    assert database_fixture.getUser(test_user.chat_id) is not None

    action = create_callback_action_message(Strings.ActionForget.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: 977,
            Strings.ActionForget.DATA_ACTION_CONFIRMED: Strings.ActionForget.ACTION_CANCEL,
            })

    wind_bot.OnWebHook(action)
    assert database_fixture.getUser(test_user.chat_id) is not None
    assert_last_message_deleted()

    action = create_callback_action_message(Strings.ActionForget.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: 977,
            Strings.ActionForget.DATA_ACTION_CONFIRMED: Strings.ActionForget.ACTION_REMOVE,
            })

    wind_bot.OnWebHook(action)
    assert database_fixture.getUser(test_user.chat_id) is None
    assert_last_message_deleted()


def test_callback_action_forget_initiation_from_command(wind_bot, telegram_env, create_webhook_message,
                                                        assert_markup_button):
    wind_bot.OnWebHook(create_webhook_message("/forget"))
    assert_sent_message(Strings.ActionForget.FORGET_ACTION_TEXT)
    assert_markup_button(Strings.Keyboard.BUTTON_YES)
