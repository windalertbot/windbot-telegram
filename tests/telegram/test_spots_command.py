import pytest

from errors import CommonErrors
from datetime import datetime

from actions.user_actions import UserActions
from telegram.callback_actions import Strings
from tests.telegram.conftest import assert_sent_message
from data.alert import AlertObjectProxy
from data.spot import SpotWeather


@pytest.fixture
def add_spot(database_fixture):
    def _add_spot(user, id):
        user.addAlert(AlertObjectProxy({
            'id': id,
            'name': 'spot name ' + id,
            'ws': 1,
            'wd': [0-359]
        }))
        assert database_fixture.updateUser(user)

    return _add_spot


@pytest.fixture
def set_weather(windprovider_fixture):
    weather = {
    }

    def _pached_current_state(ids):
        states = []
        for spot_id in ids:
            if spot_id in weather:
                states.append(SpotWeather({
                    'id': spot_id,
                    'dtl': datetime(year=2020, month=8, day=1, hour=15).isoformat(),
                    'wd': weather[spot_id].get('wd', 180),
                    'ws': weather[spot_id].get('ws', 50),
                    'wg': weather[spot_id].get('ws', 44),
                    'cl': weather[spot_id].get('cl', 0.1),
                    'at': weather[spot_id].get('at', 33),
                    'ap': weather[spot_id].get('ap', 8898)
                }))
            else:
                states.append(SpotWeather({
                    'id': spot_id,
                    'dtl': datetime(year=2020, month=8, day=1, hour=15).isoformat(),
                    'wd': 180,
                    'ws': 50,
                }))
        return states

    windprovider_fixture.getCurrentState = _pached_current_state

    def _set_weather(spot, windspeed=None, winddir=None, windgust=None, clouds=None, temp=None, pressure=None):
        weather[spot] = {}
        if windspeed:
            weather[spot]['ws'] = windspeed
        if winddir:
            weather[spot]['wd'] = winddir
        if windgust:
            weather[spot]['wg'] = windspeed
        if clouds:
            weather[spot]['cl'] = clouds
        if temp:
            weather[spot]['at'] = temp
        if pressure:
            weather[spot]['ap'] = pressure

    return _set_weather


def test_windbot_spots_command(wind_bot, telegram_env, create_webhook_message, test_user, add_spot, set_weather):
    wind_bot.OnWebHook(create_webhook_message("/alerts"))
    assert_sent_message(CommonErrors.USER_HAS_NO_ALERTS)

    add_spot(test_user, 'x')
    wind_bot.OnWebHook(create_webhook_message("/alerts"))
    assert_sent_message('spot name x')
    assert_sent_message('Wind speed: *50*')

    set_weather('x', clouds=0.1)
    wind_bot.OnWebHook(create_webhook_message("/alerts"))
    assert_sent_message(Strings.ActionWeather.TEXT_CLEAR)

    set_weather('x', clouds=0.3, winddir=77, temp=10)
    wind_bot.OnWebHook(create_webhook_message("/alerts"))
    assert_sent_message('Direction: *77˚*')
    assert_sent_message('Temp: *10*C˚')
    assert_sent_message(Strings.ActionWeather.TEXT_PARTLY_CLODY)

    set_weather('x', clouds=0.6)
    wind_bot.OnWebHook(create_webhook_message("/alerts"))
    assert_sent_message(Strings.ActionWeather.TEXT_CLOUDY)

    set_weather('x', clouds=0.8)
    wind_bot.OnWebHook(create_webhook_message("/alerts"))
    assert_sent_message(Strings.ActionWeather.TEXT_OVERCAST)


def test_windbot_spots_command_fails(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/alerts", chat_id=778797))
    assert_sent_message(UserActions.Strings.USER_NOT_FOUND_MESSAGE)
