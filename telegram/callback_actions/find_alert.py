import logging

from typing import Dict, Any

from telebot.types import InlineKeyboardMarkup
from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction
from data.spot import SpotSearchResult

from actions.spot_actions import SpotActions

logger = logging.getLogger(__name__)


class FindAlertAction(BaseAction):
    def __init__(self, message, data):
        super().__init__(message, data)
        self.search = data.get(Strings.ActionFindAlert.DATA_SEARCH_STR, None)
        self.country = data.get(Strings.ActionFindAlert.DATA_COUNTRY_CODE, None)
        self.cancel = data.get(Strings.ActionFindAlert.DATA_CANCEL, None)

    def process(self, telegram, **kwargs) -> None:
        if not self.search or len(self.search) < 4 or self.cancel:
            self._cleanup(telegram)
            if not self.cancel:
                logger.error(f"Find alert action was triggered with invalid search string:'{self.search}'")
            return

        results = SpotActions.SearchSpotByName(self.search)

        reply_args = Markup.build(results, self.search, self.country, self.origin)

        telegram.UpdateMessage(self.message, reply_args)


class Markup:
    @staticmethod
    def build(results: SpotSearchResult, search: str, country: str, origin: int) -> Dict[str, Any]:
        if not country:
            return Markup.__build_country_selection_reply(results, search, origin)
        else:
            return Markup.__build_spot_selection_reply(results, search, country, origin)

    @staticmethod
    def __build_country_selection_reply(results: SpotSearchResult, search: str, origin: str) -> Dict[str, Any]:
        markup = InlineKeyboardMarkup()

        added = []
        for spot in results.spots:
            if spot.country not in added:
                added.append(spot.country)
                markup.add(BaseAction.Markup.create_button(spot.country, Strings.ActionFindAlert.NAME, {
                    Strings.ActionFindAlert.DATA_SEARCH_STR: search,
                    Strings.ActionFindAlert.DATA_COUNTRY_CODE: spot.country,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                }))

        markup.add(BaseAction.Markup.close_button(Strings.ActionFindAlert.NAME, {
            Strings.ActionFindAlert.DATA_CANCEL: True,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
        }))

        return BaseAction.Markup.build(
            Strings.ActionFindAlert.SELECT_COUNTRY_REPLY_TEXT.format(search=search),
            markup)

    @staticmethod
    def __build_spot_selection_reply(results: SpotSearchResult, search: str, country: str,
                                     origin: str) -> Dict[str, Any]:
        markup = InlineKeyboardMarkup()

        for found_spot in results.spots:
            if found_spot.country == country:
                text = f"{found_spot.name} ({found_spot.id})"
                markup.add(BaseAction.Markup.create_button(text, Strings.ActionAddAlert.NAME, {
                    Strings.ActionAddAlert.DATA_SPOT_ID: found_spot.id,
                    Strings.ActionBase.DATA_ORIGIN_NAME: origin
                }))

        markup.add(BaseAction.Markup.back_button(Strings.ActionFindAlert.NAME, {
            Strings.ActionFindAlert.DATA_SEARCH_STR: search,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
        }))

        return BaseAction.Markup.build(
            Strings.ActionFindAlert.SELECT_SPOT_REPLY_TEXT.format(search=search, country=country),
            markup)
