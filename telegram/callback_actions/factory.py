import telebot
import logging

from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction
from telegram.callback_actions.settings import SettingsAction
from telegram.callback_actions.timezone import SetTimezoneAction
from telegram.callback_actions.notifytime import NotifytimeCallbackAction
from telegram.callback_actions.add_alert import AddAlertAction
from telegram.callback_actions.remove_alert import RemoveAlertAction
from telegram.callback_actions.find_alert import FindAlertAction
from telegram.callback_actions.forget import ForgetAction
from telegram.callback_actions.weather import WeatherAction
from telegram.callback_actions.close import CloseAction

logger = logging.getLogger(__name__)


class CallbackActionFactory:
    AVAILABLE_CALLBACKS = {
        Strings.ActionSettings.NAME: SettingsAction,
        Strings.ActionTimezone.NAME: SetTimezoneAction,
        Strings.ActionNotifytime.NAME: NotifytimeCallbackAction,
        Strings.ActionAddAlert.NAME: AddAlertAction,
        Strings.ActionRemoveAlert.NAME: RemoveAlertAction,
        Strings.ActionFindAlert.NAME: FindAlertAction,
        Strings.ActionForget.NAME: ForgetAction,
        Strings.ActionWeather.NAME: WeatherAction,
        Strings.ActionClose.NAME: CloseAction,
    }

    @staticmethod
    def create(message: telebot.types.Message, callback_data: str) -> BaseAction:
        data = BaseAction.CallbackData.Deserialize(callback_data)

        callback_action = data.get(Strings.ActionsFactory.ACTION_FIELD_NAME, None)
        if not callback_action:
            logger.error(f"Callback '{Strings.ActionsFactory.ACTION_FIELD_NAME}' param is not set.\
                Invalid callback data:\n'{data}'")
            return None

        handler = CallbackActionFactory.AVAILABLE_CALLBACKS.get(callback_action, None)
        if not handler:
            logger.error(f"There is no handler for callback action:'{callback_action}'")
            return None

        return handler(message, data)
