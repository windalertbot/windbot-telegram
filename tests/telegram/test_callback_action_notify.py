import pytest

from actions.user_actions import UserActions
from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction
from tests.telegram.conftest import assert_sent_message


@pytest.fixture
def create_set_notifytime_action(create_callback_action_message):
    def _create_set_notifytime_action(start, end, confirm=None, origin=None):
        return create_callback_action_message(Strings.ActionNotifytime.NAME, {
            Strings.ActionNotifytime.DATA_START_TIME_NAME: start,
            Strings.ActionNotifytime.DATA_END_TIME_NAME: end,
            Strings.ActionNotifytime.DATA_CONFIMED_NAME: confirm,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })
    return _create_set_notifytime_action


def test_callback_action_settings_set_notifytime_invalid(wind_bot, telegram_env, create_set_notifytime_action, caplog):
    action = create_set_notifytime_action('aa', 'bb', True)
    wind_bot.OnWebHook(action)
    assert_sent_message("Value:'[aa,bb]' has invalid notification time format")

    action = create_set_notifytime_action('11', '66', True)
    wind_bot.OnWebHook(action)
    assert_sent_message("Invalid evening value: '66'")


def test_callback_action_settings_set_notifytime_init(wind_bot, telegram_env, create_set_notifytime_action, caplog,
                                                      assert_markup_button):
    action = create_set_notifytime_action(None, None)
    wind_bot.OnWebHook(action)
    assert_sent_message(Strings.ActionNotifytime.USER_SETTINGS_NOTIFY_SET_INIT)
    assert_markup_button("0️⃣")
    assert_markup_button("❌", False)


def test_callback_action_settings_set_notifytime_start(wind_bot, telegram_env, create_set_notifytime_action, caplog,
                                                       assert_markup_button):
    action = create_set_notifytime_action('1', None)
    wind_bot.OnWebHook(action)
    assert_sent_message("1️⃣#️⃣ : 0️⃣0️⃣  ➖  #️⃣#️⃣ : 0️⃣0️⃣")
    assert_markup_button("❌", False)

    action = create_set_notifytime_action('12', None)
    wind_bot.OnWebHook(action)
    assert_sent_message("1️⃣2️⃣ : 0️⃣0️⃣  ➖  #️⃣#️⃣ : 0️⃣0️⃣")
    assert_markup_button("❌", False)


def test_callback_action_settings_set_notifytime_end(wind_bot, telegram_env, create_set_notifytime_action, caplog,
                                                     assert_markup_button, assert_markup_callback_data):
    action = create_set_notifytime_action('10', '1')
    wind_bot.OnWebHook(action)
    assert_sent_message("➖  1️⃣#️⃣ : 0️⃣0️⃣")
    assert_markup_button("❌", False)

    action = create_set_notifytime_action('08', '2')
    wind_bot.OnWebHook(action)
    assert_sent_message("0️⃣8️⃣ : 0️⃣0️⃣  ➖  2️⃣#️⃣ : 0️⃣0️⃣")
    assert_markup_button("❌", False)
    assert_markup_callback_data('08')


def test_callback_action_settings_set_notifytime_confirm(wind_bot, telegram_env, create_set_notifytime_action,
                                                         database_fixture, test_user, caplog):
    start = 11
    end = 19

    action = create_set_notifytime_action(str(start), str(end))
    wind_bot.OnWebHook(action)
    assert_sent_message(Strings.ActionNotifytime.USER_SETTINGS_NOTIFY_SET_CONFIRM.format(
        start=BaseAction.Markup.num_to_emoji(start), end=BaseAction.Markup.num_to_emoji(end)))

    action = create_set_notifytime_action(start, end, True)
    wind_bot.OnWebHook(action)
    assert_sent_message(UserActions.Strings.NOTIFYTIME_UPDATED_MESSAGE)

    user = database_fixture.getUser(test_user.chat_id)
    assert user.awake == start
    assert user.sleep == end
