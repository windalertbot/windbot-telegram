import pytest
import utils

from typing import List, Tuple
from datetime import datetime

from aws_lambda_factory import Factory

from actions.collect_spots import UserNotificationCollector
from actions.collect_weather import WeatherCollector

from data.spot import SpotWeather


@pytest.fixture
def create_spots():
    def _create_spots(ids: List[str]):
        spots = []
        for id in ids:
            spots.append({
                "id": id,
                "desired_wind_speed": 15,
                "desired_wind_direction": [180, 275],
                "name": f"Name_{id}"
            })
        return spots
    return _create_spots


@pytest.fixture
def create_weather_data():
    def _create_weather_data(data: List[Tuple[str, str, int, int]]):
        weather_data = {}
        for d in data:
            weather_data[d[0]] = SpotWeather({
                'id': d[0],
                'dtl': d[1],
                'ws': d[2] if len(d) > 2 else 50,
                'wd': d[3] if len(d) > 3 else 180,
            })
        return weather_data

    return _create_weather_data


def test_collect_spot_action_no_user_timezone(with_patched_factory, database_fixture,
                                              create_spots, create_weather_data):
    alerts = create_spots(["aa22", "bb33"])
    weather_data = create_weather_data([
        ['aa22', '2020-06-07T15:00:00-02:00'],
        ['bb33', datetime(year=2020, day=10, month=2, hour=12).isoformat()],
        ['excluded', '2020-06-07T22:00:00+08:00', 2],
        ])

    database_fixture.createUser(1, alerts=alerts)
    database_fixture.createUser(2, alerts=alerts)
    database_fixture.createUser(3, utils.parseTimezone('22'), alerts=alerts)
    u = database_fixture.getUsersToNotify()

    result = UserNotificationCollector.collect(u, weather_data)

    assert len(result) == 2
    assert len(result[1]) == 2
    assert len(result[2]) == 2
    assert result[1][0].spot_id == 'aa22'
    assert result[2][1].spot_id == 'bb33'


def test_collect_spot_action_no_spots_returned(with_patched_factory, database_fixture,
                                               create_spots, create_weather_data):
    alerts = create_spots(["excluded", "bb33"])
    weather_data = create_weather_data([
        ['excluded', '2020-06-07T22:00:00+08:00', 2],
        ])

    database_fixture.createUser(1, alerts=alerts)
    database_fixture.createUser(2, alerts=alerts)
    u = database_fixture.getUsersToNotify()
    assert len(u) == 2

    result = UserNotificationCollector.collect(u, weather_data)
    assert not result == 0


def test_collect_spot_action_no_spots_wrong_direction(with_patched_factory, database_fixture,
                                                      create_spots, create_weather_data):
    alerts1 = create_spots(["d1", "d2"])
    alerts2 = create_spots(["d1", "d2"])
    alerts2[1]['desired_wind_direction'] = [350, 10]

    weather_data = create_weather_data([
        ['d1', '2020-06-07T12:00:00+08:00', 50, 45],
        ['d2', '2020-06-07T12:00:00+08:00', 50, 330],
        ])

    database_fixture.createUser(1, alerts=alerts1)
    database_fixture.createUser(2, alerts=alerts2)

    u = database_fixture.getUsersToNotify()
    assert len(u) == 2

    result = UserNotificationCollector.collect(u, weather_data)
    assert not result == 0

    weather_data = create_weather_data([
        ['d1', '2020-06-07T12:00:00+02:00', 50, 45],
        ['d2', '2020-06-07T12:00:00+02:00', 50, 0],
        ])

    result = UserNotificationCollector.collect(u, weather_data)
    assert len(result) == 1
    assert result[2][0].spot_id == 'd2'


def test_collect_spot_action_no_user_timezone_filtered_spots(with_patched_factory,
                                                             database_fixture,
                                                             create_spots,
                                                             create_weather_data):
    alerts = create_spots(["spot_sent", "spot_filtered"])
    weather_data = create_weather_data([
        ['spot_sent', datetime(year=2020, day=10, month=2, hour=12).isoformat()],
        ['spot_filtered', '2020-06-07T09:00:00-02:00']])

    user_id = 1
    database_fixture.createUser(user_id, alerts=alerts)
    u = database_fixture.getUsersToNotify()

    result = UserNotificationCollector.collect(u, weather_data)
    assert len(result) == 1
    assert len(result[user_id]) == 1
    assert result[user_id][0].spot_id == 'spot_sent'


def test_collect_weather_action(with_patched_factory, database_fixture, create_spots):

    alerts = create_spots(['s1', 's2'])

    database_fixture.createUser(11,  "GMT-11", alerts=alerts)
    database_fixture.createUser(8,   "GMT-8",  alerts=alerts)
    database_fixture.createUser(4,   "GMT-4",  alerts=alerts)
    database_fixture.createUser(0,   "GMT",    alerts=alerts)
    database_fixture.createUser(1,             alerts=alerts)
    database_fixture.createUser(44,  "GMT+4",  alerts=alerts)
    database_fixture.createUser(88,  "GMT+8",  alerts=alerts)
    database_fixture.createUser(111, "GMT+11", alerts=alerts)

    u = database_fixture.getUsersToNotify()
    assert len(u) == 8

    result = WeatherCollector.collect(Factory.AccessWindprovider(), u)

    for users in u:
        for alert in users.alerts:
            assert alert.spot_id in result
