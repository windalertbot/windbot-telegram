import pytest
import os
import json

from telebot.types import InlineKeyboardMarkup
from tests.conftest import LAST_SENT_MESSAGE_ENV
from telegram.callback_actions.base import BaseAction

telegram_chat_id = 228


# Use via assert_message fixture
def assert_sent_message(search_text, includes=True):
    last_sent_message_text = os.getenv(LAST_SENT_MESSAGE_ENV)
    assert last_sent_message_text, "Nothing was sent, or was deleted"

    if includes:
        assert search_text in last_sent_message_text, "Text: '{0}' wasn't found in LAST_SENT_MESSAGE: '{1}'".format(
            search_text,
            last_sent_message_text)
    else:
        assert search_text not in last_sent_message_text, "Text: '{0}' wasn found in LAST_SENT_MESSAGE: '{1}'".format(
            search_text,
            last_sent_message_text)


@pytest.fixture
def assert_message():
    return assert_sent_message


@pytest.fixture
def assert_last_message_deleted():
    def _assert_last_message_deleted():
        assert not os.getenv(LAST_SENT_MESSAGE_ENV)
    return _assert_last_message_deleted


@pytest.fixture
def assert_markup_button(wind_bot):
    def _assert_markup_button(button_text, includes=True):
        assert wind_bot.LAST_MARKUP, "Nothing was sent, or was deleted"
        assert isinstance(wind_bot.LAST_MARKUP, InlineKeyboardMarkup)

        for row in wind_bot.LAST_MARKUP.keyboard:
            for button in row:
                if button_text in button.text:
                    assert includes, f"{button_text} was found in button: {button.to_json()}"
                    return

        assert not includes, f"{button_text} wasn't found in last markup: {wind_bot.LAST_MARKUP.to_json()}"

    return _assert_markup_button


@pytest.fixture
def assert_markup_callback_data(wind_bot):
    def _assert_markup_callback_data(button_text, includes=True):
        assert wind_bot.LAST_MARKUP, "Nothing was sent, or was deleted"
        assert isinstance(wind_bot.LAST_MARKUP, InlineKeyboardMarkup)

        for row in wind_bot.LAST_MARKUP.keyboard:
            for button in row:
                if button_text in button.callback_data:
                    assert includes, f"'{button_text}' was found in button's callback data: {button.to_json()}"
                    return

        assert not includes, f"'{button_text}' wasn't found in last markup: {wind_bot.LAST_MARKUP.to_json()}"

    return _assert_markup_callback_data


@pytest.fixture
def create_webhook_message():
    def _create_webhook_message(text: str, chat_id: int = telegram_chat_id):
        return """{
            "update_id":335682594,
            "message":
                {
                    "message_id":6,
                    "from":
                        {
                            "id":""" + str(chat_id) + """,
                            "is_bot":false,
                            "first_name":"name",
                            "last_name":"surname",
                            "username":"any",
                            "language_code":"ru"
                        },
                    "chat":
                        {
                            "id":""" + str(chat_id) + """,
                            "first_name":"name",
                            "last_name":"surname",
                            "username":"any",
                            "type":"private"
                        },
                    "date":1615036255,
                    "text":""" + '"{0}"'.format(text.replace('"', '\\"')) + """,
                    "entities":
                        [
                            {"offset":0,"length":6,"type":"bot_command"}
                        ]
                    }
                }
                """
    return _create_webhook_message


@pytest.fixture
def create_callback_action_message():
    def _ceate_callback_action_message(action, payload, data=None, markup=None, chat_id: int = telegram_chat_id):
        if not data:
            data_str = BaseAction.CallbackData.Serialize(action, payload).replace('"', '\\"')
        else:
            data_str = json.dumps(data).replace('"', '\\"')[1:-1]

        markup_str = ""
        if markup is not None:
            markup_str = f', "reply_markup": {json.dumps(markup)}'

        return """
            {
                "update_id": 335682671,
                "callback_query": {
                        "id": "1230624655248332842",
                        "from": {
                            "id": """ + str(chat_id) + """,
                            "is_bot": false,
                            "first_name": "name",
                            "last_name": "surname",
                            "username": "any",
                            "language_code": "ru"
                        },
                        "message": {
                            "message_id": 129,
                            "from": {
                                "id": 1630635008,
                                "is_bot": true,
                                "first_name": "Wind Alert",
                                "username": "WindInfoBot"
                            },
                            "chat": {
                                "id": """ + str(chat_id) + """,
                                "first_name": "name",
                                "last_name": "surname",
                                "username": "any",
                                "type": "private"
                            },
                            "date": 1615817862,
                            "text": "test: create_callback_action_message()"
                            """ + markup_str + """
                        },
                    "chat_instance": "5553725106197632857",
                    "data": """ + f'"{data_str}"' + """
                }
            }
        """
    return _ceate_callback_action_message


@pytest.fixture()
def with_user_in_database(database_fixture):
    database_fixture.createUser(telegram_chat_id)


@pytest.fixture()
def test_user(database_fixture):
    return database_fixture.getUser(telegram_chat_id)


@pytest.fixture
def telegram_env(with_patched_factory, with_user_in_database):
    pass
