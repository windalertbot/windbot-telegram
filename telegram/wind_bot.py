import telebot
import shlex
import logging

from typing import List, Dict, Any, Union

from telebot.apihelper import ApiTelegramException
from errors import TelegramErrorCode, CommonErrors

from telegram.message_utils import (
    create_welcome_reply, create_help_message, create_search_reply, create_spots_message,
    create_settings_message_reply, create_spots_message_text)
from telegram.utils import TelegramApiMethods, TelegramMessageFields, create_message_payload

from interfaces import IWindbot
from telegram.callback_actions.factory import CallbackActionFactory
from telegram.callback_actions.add_alert import Markup as AddAlertMarkup
from telegram.callback_actions.remove_alert import Markup as RemoveAlertMarkup
from telegram.callback_actions.forget import Markup as ForgetUserMarkup
from telegram.callback_actions.close import Markup as CloseMarkup
from telegram.callback_actions.base import BaseAction

from actions.user_actions import UserActions
from actions.spot_actions import SpotActions

from data.alert import AlertObjectProxy

logger = logging.getLogger(__name__)


class WindBot(telebot.TeleBot, IWindbot):
    WEBHOOK_REPLIES_ENABLED = True

    def __init__(self, token):
        super().__init__(token, threaded=False, num_threads=1)
        self.__webhook_reply = {}

        @self.callback_query_handler(func=lambda call: True)
        def handle_query(call):
            handler = CallbackActionFactory.create(call.message, call.data)
            if handler:
                handler.process(self)

        @self.message_handler(commands=['start'])
        def send_welcome(message: telebot.types.Message):
            user = UserActions.GetUser(message.chat.id)
            if not user:
                UserActions.CreateUser(message.chat.id)

            response = create_welcome_reply(user, message)
            self.SendMessage(message.chat.id, **response)

        @self.message_handler(commands=['help'])
        def send_help(message: telebot.types.Message):
            response = CloseMarkup.build(create_help_message(message), message.id)
            response['disable_web_page_preview'] = True
            self.SendMessage(message.chat.id, **response)

        @self.message_handler(commands=['find'])
        def find_spot(message: telebot.types.Message):
            arguments = self.__getCommandArguments(message.text)
            while True:
                if len(arguments) < 1:
                    response = BaseAction.Markup.build("Usage: `/find <name of spot>`")
                    break

                search_string = ' '.join(arguments)
                if len(search_string) < 4:
                    response = BaseAction.Markup.build("You need to enter at least 4 symbols to search... 🤖")
                    break

                if len(search_string) > 16:
                    response = BaseAction.Markup.build("Search string is too big! 🤖")
                    break

                results = SpotActions.SearchSpotByName(search_string)
                response = create_search_reply(message, search_string, results)
                break

            self.SendMessage(message.chat.id, **response)

        @self.message_handler(commands=['alert'])
        def set_alert(message: telebot.types.Message):

            arguments = self.__getCommandArguments(message.text)

            # enter interactive mode
            if len(arguments) == 1:
                spot_info = SpotActions.GetSpotInfo(arguments[0])
                if not spot_info:
                    response = BaseAction.Markup.build(
                        UserActions.Strings.SPOT_NOT_FOUND_MESSAGE)
                else:
                    response = AddAlertMarkup.build(None, spot_info.id, None, None, message.id)
            elif len(arguments) < 3:
                response = BaseAction.Markup.build("Usage: `/alert <ID> <knots> <wind direction> `")
            else:
                response = BaseAction.Markup.build(
                    UserActions.AddAlert(message.chat.id, arguments[0], arguments[1], arguments[2]))

            self.SendMessage(message.chat.id, **response)

        @self.message_handler(commands=['settings'])
        def get_settings(message: telebot.types.Message):

            arguments = self.__getCommandArguments(message.text)
            if len(arguments) == 0:
                user = UserActions.GetUser(message.chat.id)
                if not user:
                    response = BaseAction.Markup.build(UserActions.Strings.USER_NOT_FOUND_MESSAGE)
                else:
                    # show settings and enter interactive mode
                    response = create_settings_message_reply(user, message.id)
            else:
                # command with arguments
                text = self.__doSetUserSettings(arguments, message)
                response = BaseAction.Markup.build(text)

            self.SendMessage(message.chat.id, **response)

        @self.message_handler(commands=['remove'])
        def remove_alert(message: telebot.types.Message):
            arguments = self.__getCommandArguments(message.text)

            if len(arguments) < 1:
                user = UserActions.GetUser(message.chat.id)
                if not user:
                    response = BaseAction.Markup.build(UserActions.Strings.USER_NOT_FOUND_MESSAGE)
                elif len(user.alerts) > 0:
                    response = RemoveAlertMarkup.build(user, None, message.id)
                else:
                    response = BaseAction.Markup.build(CommonErrors.USER_HAS_NO_ALERTS)
            else:
                result = UserActions.RemoveAlert(message.chat.id, arguments[0])
                response = BaseAction.Markup.build(result)

            self.SendMessage(message.chat.id, **response)

        @self.message_handler(commands=['spots', 'alerts'])
        def spots_info_command(message: telebot.types.Message):
            user = UserActions.GetUser(message.chat.id)
            if not user:
                response = BaseAction.Markup.build(UserActions.Strings.USER_NOT_FOUND_MESSAGE)
            elif not user.alerts:
                response = BaseAction.Markup.build(CommonErrors.USER_HAS_NO_ALERTS)
            else:
                response = CloseMarkup.build(
                    create_spots_message_text(message, user, UserActions.GetUserWeather(user)),
                    message.id)
            self.SendMessage(message.chat.id, **response)

        @self.message_handler(commands=['forget'])
        def forget_command(message: telebot.types.Message):
            response = ForgetUserMarkup.build(message.id)
            self.SendMessage(message.chat.id, **response)

    def OnWebHook(self, updates_json: str) -> Union[None, Dict[str, Any]]:
        self.__webhook_reply = {}

        update = telebot.types.Update.de_json(updates_json)
        self.process_new_updates([update])
        return self.__webhook_reply

    def SendNotifications(self, notifications: Dict[int, List[AlertObjectProxy]]):
        users_to_notify = UserActions.GetUsers(list(notifications.keys()))

        for user in users_to_notify:
            try:
                altered_spots = notifications[user.chat_id]
                self.SendMessage(user.chat_id, create_spots_message(altered_spots), False)

                user.snooze()
                UserActions.UpdateUser(user)

            except ApiTelegramException as apie:
                if apie.error_code == TelegramErrorCode.TOO_MANY_REQUESTS:
                    # Need reschedule update
                    logger.error("Telegram sending messages limit reached..." +
                                 f"Users left without notification:{len(users_to_notify)}")
                    return
                else:
                    # could be block exception or whatever - need to remove user
                    logger.error(
                        f"Telegram Api Error happened when tried to send notification to:{user.chat_id}\n{apie}")

            except Exception as e:
                logger.error(f"Error happened when tried to send notification to:{user.chat_id}\n{e}")

    def _createWebhookReply(self, api_action: str, **kwargs) -> bool:
        if WindBot.WEBHOOK_REPLIES_ENABLED:
            self.__webhook_reply = create_message_payload(**kwargs)
            if not self.__webhook_reply:
                return False

            self.__webhook_reply['method'] = api_action
        return WindBot.WEBHOOK_REPLIES_ENABLED

    def __getCommandArguments(self, text: str) -> List[str]:
        return shlex.split(text)[1:]

    def __doSetUserSettings(self, arguments: List[str], message: telebot.types.Message) -> str:
        if len(arguments) == 0:
            return "__doSetUserSettings() internal error"

        for arg in arguments:
            kv = arg.split('=')

            if len(kv) < 2:
                return "Unrecognized arguments:'{0}'. Must be 'key=value' 😕".format(arg)

            key, s_value = kv[0].lower(), kv[1]
            if key == 'notify':
                return UserActions.SetUserNotifytime(message.chat.id, s_value)
            elif key == 'timezone':
                return UserActions.SetUserTimezone(message.chat.id, s_value)
            else:
                return "Unknown key:'{0}' 😕".format(key)

    # IWindbot interface

    def DeleteMessage(self, chat_id: int, message_id: int, can_reply: bool = True):
        if can_reply and not self.__webhook_reply:
            if self._createWebhookReply(TelegramApiMethods.DELETE_MESSAGE, chat_id=chat_id, message_id=message_id):
                logger.debug(f"Message will be deleted as webhook reply: {self.__webhook_reply}")
                return

        self.delete_message(chat_id, message_id)

    def SendMessage(self, chat_id: int, text: str, can_reply: bool = True, **kwargs):
        logger.debug(f"Sending message to:{chat_id}\n'{text}'")

        if can_reply and not self.__webhook_reply:
            if self._createWebhookReply(TelegramApiMethods.SEND_MESSAGE, chat_id=chat_id, text=text, **kwargs):
                logger.debug(f"Message will be sent as webhook reply: {self.__webhook_reply}")
                return

        return self.send_message(chat_id, kwargs.get('text', text), **kwargs)

    def UpdateMessage(self, message: telebot.types.Message, edit_args: Dict[str, Any], can_reply: bool = True):
        text = edit_args.get(TelegramMessageFields.TEXT, None)
        reply_markup = edit_args.get(TelegramMessageFields.REPLY_MARKUP, None)
        parse_mode = edit_args.get(TelegramMessageFields.PARSE_MODE, 'Markdown')

        chat_id = message.chat.id
        message_id = message.id

        if can_reply and not self.__webhook_reply:
            api_action = TelegramApiMethods.EDIT_MESSAGE if text else TelegramApiMethods.EDIT_MESSAGE_MARKUP
            if self._createWebhookReply(api_action,
                                        chat_id=chat_id,
                                        message_id=message_id,
                                        text=text,
                                        parse_mode=parse_mode,
                                        reply_markup=reply_markup):
                logger.debug(f"Message will be updated as webhook reply: {self.__webhook_reply}")
                return

        if text:
            self.edit_message_text(text, chat_id, message_id, parse_mode=parse_mode, reply_markup=reply_markup)
        else:
            self.edit_message_reply_markup(chat_id, message_id, parse_mode=parse_mode, reply_markup=reply_markup)
