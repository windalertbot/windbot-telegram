from typing import Dict, Any

from telebot.types import InlineKeyboardMarkup

from interfaces import IWindbot

from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction


class SettingsAction(BaseAction):
    def __init__(self, message, data):
        super().__init__(message, data)
        self.do_action = data.get(Strings.ActionSettings.DATA_DO_KEY, None)

    def process(self, telegram: IWindbot, **kwargs) -> Dict[str, Any]:
        if not self.do_action:
            self._cleanup(telegram)

        elif self.do_action == Strings.ActionSettings.DATA_SHOW_NAME:
            self._show_settings(telegram)
        else:
            # re-create reply markup for message
            reply_args = Markup.build(self.origin)

            # edit message
            telegram.UpdateMessage(self.message, reply_args)


class Markup:
    @staticmethod
    def build(origin: int) -> Dict[str, Any]:
        markup = InlineKeyboardMarkup()

        button_timezone = BaseAction.Markup.create_button('🕔 Timezone', Strings.ActionTimezone.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })

        button_notifytime = BaseAction.Markup.create_button('⏰ Notification time', Strings.ActionNotifytime.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })

        button_removealert = BaseAction.Markup.create_button('🔕 Remove alert', Strings.ActionRemoveAlert.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })

        button_back = BaseAction.Markup.back_button(Strings.ActionSettings.NAME, {
            Strings.ActionSettings.DATA_DO_KEY: Strings.ActionSettings.DATA_SHOW_NAME,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin
            })

        markup.row(button_timezone).row(button_notifytime).row(button_removealert).row(button_back)

        return BaseAction.Markup.build(
            Strings.ActionSettings.USER_SETTINGS_CALLBACK_SELECT_EDIT_OPTION,
            markup)
