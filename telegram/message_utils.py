import telebot
import version

from typing import List, Dict, Any

from errors import CommonErrors

from telebot import types

from data.alert import AlertObjectProxy
from data.user import UserObjectProxy

from telegram.callback_actions import Strings
from telegram.callback_actions.base import BaseAction

from data.spot import SpotSearchResult, SpotWeather


USER_SETTINGS_MESSAGE_FORMAT = """Your settings:

*Timezone:* 🕓 {tz}
  Your timezone. If not set - *notify* time will be relative spot's timezone.

*Notify:* 🔔 from {morning}:00 till {evening}:00
  Time when bot is allowed to send you messages.

*Alerts:*
{alerts}"""

USER_SETTINGS_ALERTS_MESSAGE_FORMAT = """`  `📍 *{name}*  Id: *{id}*  wind speed: *{ws}*  \
wind direction: from *{wd_s}˚* to *{wd_f}˚*

"""

USER_SPOTS_WEATHER_FORMAT = """
Current weather info of your spots.
{weather}
"""

SPOT_WEATHER_FORMAT = """
  📍 *{name}*
`     `🌬 Wind speed: *{speed}* (*{gusts}*) kts. Direction: *{direction}˚*.
`     `🌡 Temp: *{temp}*C˚. Pressure: {pressure}hPa.
`     `{clouds}
"""

USER_HELP_MESSAGE_FORMAT = """Wind Alert Bot. Verion: {version}\n
This telegram bot is designed to alert you (send private message) when some wind/kite/surfing spot has wind.
Or any other kind of spot.

🔸 Find the spot:
    `/find name` - you don't need to know exact name
        of the spot, but at least 4 symbols. Please note:
        there is no specific format of spot names, they may
        not contain a city or a beach name in themselves.

🔸 Create spot alert:
    `/alert us1234 15 180-320`
    `/alert us1234 15 S-NW`
      ▪️ _us1234_ - spot ID
      ▪️ _15_ - minimal wind speed in knots
      ▪️ _180-320 (S-NW)_ - desired wind direction in degrees or symbols
         (S - South, SW - SouthWest, WSW - WestSouthWest, etc).
         Clockwise, 315-45 means any direction from NW to NE (90˚).

    `/alert us1234` - launch interactive add mode for specific id.

🔸 Get current weather information of spots you've set alerts:
    `/spots`

🔸 Remove alert:
    `/remove us1234`
      ▪️ _us1234_ - spot ID

🔸 Show your settings:
    `/settings`

🔸 Update your settings:
    `/settings key=value`

    *Timezone*
      `timezone=GMT+5` - set timezone explicitly.
        ▪️ _GMT+5_ - the timezone to be set.
      `timezone=22` - set timezone according to your time.
        ▪️ _22_ - is your local hour right now.

    *Notification Time*
      `notify=[10,22]` - set notification hours.
        ▪️ _[10,22]_ - from 10:00 to 22:00

🔸 Ask bot to forget you (GDPR and stuff):
    `/forget` - this command will remove you from database.
      The only personal data we store is `id` of the telgram chat.
      If you don't want to use bot anymore - use this command


Almost every command is interactive ⌨️. You can easily do some actions just via UI buttons.
For support, suggestions or any other question, please [contact author](https://forms.gle/QxQq62bfJ4MBHH2T7).

"""

USER_WELCOME_MESSAGE = """Welcome back, {Name}! 🤙
/help for details"""

USER_WELCOME_MESSAGE_FORMAT_NO_TIMEZONE = """Hello, {Name}! 🤙
Please set up your timezone.

/help for details"""


WIND_FOUND_MESSAGE_PRE = "💨 Wow! 💨\n"
WIND_FOUND_MESSAGE_FORMAT = """There is wind in {name}! {wind} knots!"""


def create_spots_message(alerts: List[AlertObjectProxy]) -> str:
    if len(alerts) <= 0:
        return "Bot became insane..."

    message = WIND_FOUND_MESSAGE_PRE
    for alert in alerts:
        message += WIND_FOUND_MESSAGE_FORMAT.format(
            name=alert.spot_name, wind=alert.weather_data.wind_speed)

    return message


def create_help_message(message: telebot.types.Message) -> str:
    return USER_HELP_MESSAGE_FORMAT.format(version=version.VERSION)


def create_welcome_reply(user: UserObjectProxy, message: telebot.types.Message) -> str:
    if not user or not user.timezone:
        text = USER_WELCOME_MESSAGE_FORMAT_NO_TIMEZONE.format(Name=message.from_user.first_name)

        markup = types.InlineKeyboardMarkup()
        button_set_timezone = BaseAction.Markup.create_button('🕓 Setup timezone', Strings.ActionTimezone.NAME, {
            Strings.ActionBase.DATA_ORIGIN_NAME: message.id
            })

        markup.row(button_set_timezone)

        return BaseAction.Markup.build(text, markup)
    else:
        return BaseAction.Markup.build(USER_WELCOME_MESSAGE.format(Name=message.from_user.first_name))


def create_settings_message_reply(user: UserObjectProxy, origin: int, additional_text: str = None) -> Dict[str, Any]:
    """
        Create callback reply dict with markup and text.
            When user sent /settings command
        @origin is original message id to be deleted after cancellation
    """
    if not user:
        raise NameError('argument `user` is none')

    alerts = ""
    for alert in user.alerts:
        alerts += USER_SETTINGS_ALERTS_MESSAGE_FORMAT.format(
            name=alert.spot_name,
            id=alert.spot_id,
            ws=alert.desired_wind_speed,
            wd_s=alert.desired_wind_direction[0],
            wd_f=alert.desired_wind_direction[1]
        )

    text = USER_SETTINGS_MESSAGE_FORMAT.format(
            morning=user.awake,
            evening=user.sleep,
            alerts=alerts,
            tz=user.timezone)

    if additional_text:
        text += "\n\n" + additional_text

    markup = types.InlineKeyboardMarkup()

    button_edit = types.InlineKeyboardButton(
        text='🔶 Edit 🔶',
        callback_data=BaseAction.CallbackData.Serialize(Strings.ActionSettings.NAME, {
            Strings.ActionSettings.DATA_DO_KEY: Strings.ActionSettings.DATA_EDIT_NAME,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin,
        }))

    button_close = types.InlineKeyboardButton(
        text='❎ Close ❎',
        callback_data=BaseAction.CallbackData.Serialize(Strings.ActionSettings.NAME, {
            Strings.ActionSettings.DATA_DO_KEY: None,
            Strings.ActionBase.DATA_ORIGIN_NAME: origin,
        }))

    markup.row(button_edit).row(button_close)
    return BaseAction.Markup.build(text, markup)


def create_search_reply(message: telebot.types.Message, search_string: str, results: SpotSearchResult) -> str:
    if results is None or len(results.spots) == 0:
        return BaseAction.Markup.build("Nothing was found...")

    reply = "Here what I've found:"

    group_by_country = {}
    for found_spot in results.spots:
        if found_spot.country in group_by_country:
            group_by_country[found_spot.country].append(found_spot)
        else:
            group_by_country[found_spot.country] = [found_spot]

    for country in group_by_country:
        reply += f"\nCountry: *{country}*\n"
        for found_spot in group_by_country[country]:
            reply += f"    *ID:* {found_spot.id}    *Name:* {found_spot.name}\n"

    markup = types.InlineKeyboardMarkup()

    if len(results.spots) == 1:
        # Only 1 result - add alert
        markup.add(BaseAction.Markup.create_button('⏰ Add alert', Strings.ActionAddAlert.NAME, {
            Strings.ActionAddAlert.DATA_SPOT_ID: results.spots[0].id,
            Strings.ActionBase.DATA_ORIGIN_NAME: message.id
        }))
    else:
        # more than 1 result

        preselected_country = None
        if len(group_by_country.keys()) == 1:
            # check if there was only 1 country
            preselected_country = list(group_by_country.keys())[0]

        markup.add(BaseAction.Markup.create_button('⏰ Add alert', Strings.ActionFindAlert.NAME, {
            Strings.ActionFindAlert.DATA_SEARCH_STR: search_string,
            Strings.ActionFindAlert.DATA_COUNTRY_CODE: preselected_country,
            Strings.ActionBase.DATA_ORIGIN_NAME: message.id
        }))

    return BaseAction.Markup.build(reply, markup)


def create_spots_message_text(message: telebot.types.Message, user: UserObjectProxy,
                              weather: List[SpotWeather]) -> Dict[str, Any]:
    if not weather:
        return CommonErrors.NO_WEATHER_DATA

    def _sky_state(cloud_cover: float):
        if cloud_cover is None:
            return ""
        if cloud_cover < 0.25:
            return Strings.ActionWeather.TEXT_CLEAR
        elif cloud_cover < 0.5:
            return Strings.ActionWeather.TEXT_PARTLY_CLODY
        elif cloud_cover < 0.75:
            return Strings.ActionWeather.TEXT_CLOUDY
        return Strings.ActionWeather.TEXT_OVERCAST

    def show(x):
        return x if x else "N/A"

    weather_info = ''
    for entry in weather:
        alert = user.getAlert(entry.id)
        weather_info += SPOT_WEATHER_FORMAT.format(
            name=alert.spot_name if alert else "N/A",
            speed=show(entry.wind_speed),
            gusts=show(entry.wind_gusts),
            direction=show(entry.wind_direction),
            temp=show(entry.air_temperature),
            pressure=show(entry.air_pressure),
            clouds=_sky_state(entry.cloud_cover)
        )

    return USER_SPOTS_WEATHER_FORMAT.format(weather=weather_info)
