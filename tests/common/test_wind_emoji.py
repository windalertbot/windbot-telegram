import utils


def assert_wdirs(target, include, exclude):
    for i in include:
        assert i in target

    for e in exclude:
        assert e not in target


def test_wind_dir_to_emoji():
    assert_wdirs(
        utils.getWindDirEmoji("N-S"),
        [
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.NE,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
        ],
        [
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
            utils.WindDirectionsEmoji.NW,
        ]
    )

    assert_wdirs(
        utils.getWindDirEmoji("37-178"),
        [
            utils.WindDirectionsEmoji.NE,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
        ],
        [
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
            utils.WindDirectionsEmoji.NW,
        ]
    )

    assert_wdirs(
        utils.getWindDirEmoji("165-310"),
        [
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ],
        [
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.NW,
            utils.WindDirectionsEmoji.NE,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
        ]
    )

    assert_wdirs(
        utils.getWindDirEmoji("315-45"),
        [
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.NW,
            utils.WindDirectionsEmoji.NE,
        ],
        [
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ]
    )

    assert_wdirs(
        utils.getWindDirEmoji("45-315"),
        [
            utils.WindDirectionsEmoji.NW,
            utils.WindDirectionsEmoji.NE,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ],
        [
            utils.WindDirectionsEmoji.N,
        ]
    )


def test_wind_dir_to_emoji_incomplete_number():
    assert_wdirs(
        utils.getWindDirEmoji("45"),
        [
            utils.WindDirectionsEmoji.NE,
        ],
        [
            utils.WindDirectionsEmoji.NW,
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ]
    )

    assert_wdirs(
        utils.getWindDirEmoji("44"),
        [
            utils.WindDirectionsEmoji.NE,
        ],
        [
            utils.WindDirectionsEmoji.NW,
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ]
    )
    assert_wdirs(
        utils.getWindDirEmoji("46"),
        [
            utils.WindDirectionsEmoji.E,
        ],
        [
            utils.WindDirectionsEmoji.NE,
            utils.WindDirectionsEmoji.NW,
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ]
    )


def test_wind_dir_to_emoji_incomplete_string():
    assert_wdirs(
        utils.getWindDirEmoji("N"),
        [
            utils.WindDirectionsEmoji.N,
        ],
        [
            utils.WindDirectionsEmoji.NW,
            utils.WindDirectionsEmoji.NE,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ]
    )

    assert_wdirs(
        utils.getWindDirEmoji("NW"),
        [
            utils.WindDirectionsEmoji.NW,
        ],
        [
            utils.WindDirectionsEmoji.NE,
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ]
    )

    assert_wdirs(
        utils.getWindDirEmoji("SSW"),
        [
            utils.WindDirectionsEmoji.SW,
        ],
        [
            utils.WindDirectionsEmoji.W,
            utils.WindDirectionsEmoji.NW,
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.NE,
        ]
    )
    assert_wdirs(
        utils.getWindDirEmoji("WNW"),
        [
            utils.WindDirectionsEmoji.NW,
        ],
        [
            utils.WindDirectionsEmoji.NE,
            utils.WindDirectionsEmoji.E,
            utils.WindDirectionsEmoji.N,
            utils.WindDirectionsEmoji.SE,
            utils.WindDirectionsEmoji.S,
            utils.WindDirectionsEmoji.SW,
            utils.WindDirectionsEmoji.W,
        ]
    )
