from typing import Tuple

from decimal import Decimal
from data.common import JsonObjectProxy
from data.spot import SpotWeather


class AlertObjectProxy(JsonObjectProxy):
    def __init__(self, json):
        super().__init__(json)
        self._weather_data = None

    @property
    def spot_id(self) -> str:
        return self['id']

    @property
    def spot_name(self) -> str:
        return self['name']

    @property
    def desired_wind_speed(self) -> int:
        return self['desired_wind_speed']

    @property
    def desired_wind_direction(self) -> Tuple[Decimal, Decimal]:
        return self['desired_wind_direction']

    # NON SERIALAZABLE
    @property
    def weather_data(self) -> SpotWeather:
        return self._weather_data

    @weather_data.setter
    def weather_data(self, value: SpotWeather):
        self._weather_data = value
