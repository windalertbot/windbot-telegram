
from tests.telegram.conftest import assert_sent_message


def test_windbot_start_command(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/start"))
    assert_sent_message('Please set up your timezone')

    wind_bot.OnWebHook(create_webhook_message("/settings"))
    # must not contain user not found in db message
    assert_sent_message('Your settings:')


def test_windbot_start_command_without_user(with_patched_factory, wind_bot, database_fixture, create_webhook_message):
    """
        no telegram_env because it contains user by default
    """
    from tests.telegram.conftest import telegram_chat_id

    assert database_fixture.getUser(telegram_chat_id) is None
    wind_bot.OnWebHook(create_webhook_message("/start"))
    assert database_fixture.getUser(telegram_chat_id) is not None


def test_windbot_help_command(wind_bot, telegram_env, create_webhook_message):
    wind_bot.OnWebHook(create_webhook_message("/help"))
    assert_sent_message('This telegram bot is designed to alert')
