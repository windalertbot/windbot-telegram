import logging
import utils
import aws_lambda_factory

from typing import Union, List

from errors import ParseError

from actions.spot_actions import SpotActions

from database.database import Database
from data.user import UserObjectProxy
from data.alert import AlertObjectProxy

from data.spot import SpotWeather


logger = logging.getLogger(__name__)


class UserActions:
    ######################################################################################################
    class Strings:
        USER_NOT_FOUND_MESSAGE = """I'm sorry. You wasn't found in database.
Try to send /start command again...\n
Or we may have some technical issues 😬"""

        USER_UPDATE_FAILED_MESSAGE = "Update setting failed. Something went wrong with database... 😵"

        TIMEZONE_UPDATED_MESSAGE = "Timezone updated 👍"
        INVALID_TIMEZONE_FORMAT_MESSAGE = "Value:'{timezone}' is invalid timezone 😕"

        NOTIFYTIME_UPDATED_MESSAGE = "Notification time updated 👍"
        INVALID_NOTIFYTIME_FORMAT_MESSAGE = "Value:'{notifytime}' has invalid notification time format 😕"

        SPOT_NOT_FOUND_MESSAGE = """The spot with such ID is unknown 🤨. Please check it again.
If you don't know the id of spot - use /find command."""

        SPOT_LIMIT_REACHED_MESSAGE = """I'm sorry you are not allowed to have one more alert on a spot. 🥺
This limitation is artificial, in future it will be removed.
Use /remove command to remove one of your current spots."""
    ######################################################################################################

    @staticmethod
    def GetUsers(ids: List[int]) -> List[UserObjectProxy]:
        return UserActions.database().getUsersBatch(ids)

    @staticmethod
    def GetUser(identifier: Union[int, UserObjectProxy]) -> UserObjectProxy:
        if isinstance(identifier, int):
            return UserActions.database().getUser(identifier)
        if isinstance(identifier, UserObjectProxy):
            return identifier
        raise ValueError(f"'{identifier}' is not chat id, neither user object")

    @staticmethod
    def UpdateUser(user: UserObjectProxy) -> bool:
        return UserActions.database().updateUser(user)

    @staticmethod
    def CreateUser(chat_id: int) -> None:
        UserActions.database().createUser(chat_id)

    @staticmethod
    def RemoveUser(chat_id: int) -> None:
        UserActions.database().removeUser(chat_id)

    @staticmethod
    def RemoveAlert(identifier: Union[int, UserObjectProxy], spot_id: str) -> str:
        user = UserActions.GetUser(identifier)
        if not user:
            return UserActions.Strings.USER_NOT_FOUND_MESSAGE

        spot = user.delAlert(spot_id)
        if not spot:
            return f"Alert with id '{spot_id}' wasn't even found in your settings 😕"

        if UserActions.UpdateUser(user):
            return "Alert was removed 👌"
        else:
            return UserActions.Strings.USER_UPDATE_FAILED_MESSAGE

    @staticmethod
    def AddAlert(identifier: Union[int, UserObjectProxy], spot_id: str, wspeed: str, wdir: str) -> str:

        spotInfo = SpotActions.GetSpotInfo(spot_id)
        if spotInfo is None:
            return UserActions.Strings.SPOT_NOT_FOUND_MESSAGE

        try:
            wind_speed = int(wspeed)
        except Exception:
            return "Wind speed must be number. 😕"

        try:
            wind_dir = utils.parseWindDir(wdir)
        except ParseError as e:
            return e.message

        user = UserActions.GetUser(identifier)
        if not user:
            return UserActions.Strings.USER_NOT_FOUND_MESSAGE

        # TODO: Check if we are updating spot
        if len(user.alerts) >= UserObjectProxy.ALERTS_LIMIT:
            return UserActions.Strings.SPOT_LIMIT_REACHED_MESSAGE

        spot = AlertObjectProxy({
            'id': spot_id,
            'name': spotInfo.name,
            'desired_wind_speed': wind_speed,
            'desired_wind_direction': wind_dir
        })

        user.addAlert(spot)

        if UserActions.UpdateUser(user):
            return "Alert was set 👌"
        else:
            return "Alert set failed. Something went wrong with database... ☠️"

    @staticmethod
    def SetUserTimezone(identifier: Union[int, UserObjectProxy], timezone: str) -> str:
        try:
            value = utils.parseTimezone(timezone)

            user = UserActions.GetUser(identifier)
            if not user:
                return UserActions.Strings.USER_NOT_FOUND_MESSAGE
            user['timezone'] = value

        except ParseError as pe:
            return pe.message
        except Exception as e:
            logger.error(f"Can't setuser timezone with:'{timezone}'.\nUnknown error:{e}")
            return UserActions.Strings.INVALID_TIMEZONE_FORMAT_MESSAGE.format(timezone=timezone)

        if UserActions.UpdateUser(user):
            return UserActions.Strings.TIMEZONE_UPDATED_MESSAGE
        else:
            return UserActions.Strings.USER_UPDATE_FAILED_MESSAGE

    @staticmethod
    def SetUserNotifytime(identifier: Union[int, UserObjectProxy], notifytime: str) -> str:
        try:
            value = utils.parseNotificationTime(notifytime)

            user = UserActions.GetUser(identifier)
            if not user:
                return UserActions.Strings.USER_NOT_FOUND_MESSAGE
            user['notify'] = value

        except ParseError as pe:
            return pe.message
        except Exception as e:
            logger.error(f"Can't set user's notification time with:'{notifytime}'.\nUnknown error:{e}")
            return UserActions.Strings.INVALID_NOTIFYTIME_FORMAT_MESSAGE.format(notifytime=notifytime)

        if UserActions.UpdateUser(user):
            return UserActions.Strings.NOTIFYTIME_UPDATED_MESSAGE
        else:
            return UserActions.Strings.USER_UPDATE_FAILED_MESSAGE

    @staticmethod
    def GetUserWeather(identifier: Union[int, UserObjectProxy]) -> List[SpotWeather]:
        user = UserActions.GetUser(identifier)
        if not user:
            logger.error(f"GetUserWeather was called with invalid user identifier: {identifier}")
            return None

        return SpotActions.GetSpotWeather([a.spot_id for a in user.alerts])

    @staticmethod
    def database() -> Database:
        return aws_lambda_factory.Factory.AccessDatabase()
