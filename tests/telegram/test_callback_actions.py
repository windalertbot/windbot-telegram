from telegram.callback_actions import Strings


def test_callback_action_invalid(wind_bot, telegram_env, create_callback_action_message, caplog):
    action = create_callback_action_message(None, None, data={
        'wrong': 'data'
        })
    wind_bot.OnWebHook(action)
    assert 'ERROR' in caplog.text
    assert f"Callback '{Strings.ActionsFactory.ACTION_FIELD_NAME}' param is not set" in caplog.text


def test_callback_action_unknown(wind_bot, telegram_env, create_callback_action_message, caplog):
    action = create_callback_action_message('unknown', {})
    wind_bot.OnWebHook(action)
    assert 'ERROR' in caplog.text
    assert "There is no handler for callback action" in caplog.text


def test_callback_action_data_invalid(wind_bot, telegram_env, create_callback_action_message, caplog):
    try:
        action = create_callback_action_message(Strings.ActionNotifytime.NAME, {
            Strings.ActionsFactory.ACTION_FIELD_NAME: 'duplicate key'
        })
        wind_bot.OnWebHook(action)
        assert False, "The data was invalid but there were no exceptions"
    except KeyError as e:
        assert f"Invalid key '{Strings.ActionsFactory.ACTION_FIELD_NAME}' in callback data" in str(e)
